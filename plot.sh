#!/bin/sh

#
# Runs the simulation and make plots
#
# Example:
#
#    ./plot.sh
#

# Variable numbers
# -----------

var_position=1
var_speed=2
var_mass=3
var_smoothing_length=4
var_density=5
var_internal_energy=6
var_pressure=7
var_sound_speed=8

FONT_NAME="Tahoma"
splash_settings=../plotting/splash_settings
splash_defaults=$splash_settings/splash.defaults
splash_limits=$splash_settings/splash.limits

# Build the simulation program
make

# Run the simulation
./build/main tmp \
  --output_dir=output \
  --output_file_basename_length=8 \
  --no_ghosts

mkdir -p plots
cd plots

# Copy column names files
cp $splash_settings/splash.columns ./

# Step 3: initial velocity
# ---------

echo '----------- Step 3: initial velocity -----------'
echo 'Initial particle velocity' > splash.titles
GIZA_FONT=$FONT_NAME splash -x $var_position -y $var_speed -dev 003_001_velocity.pdf -d $splash_defaults -l $splash_limits ../output/00000001.csv

# Step 4: initial density without ghosts
# ---------

echo '----------- Step 4: initial density without ghosts -----------'
echo 'Initial particle density' > splash.titles
GIZA_FONT=$FONT_NAME splash -x $var_position -y $var_density -dev 004_001_density.pdf -d $splash_defaults -l $splash_limits ../output/00000001.csv


# Step 5: density with ghosts
# ---------

echo '----------- Step 5: density with ghosts -----------'

# Run the simulation
rm ../output/00000001.csv

../build/main tmp \
  --output_dir=../output \
  --output_file_basename_length=8

echo "Initial particle density with ghosts" > splash.titles
GIZA_FONT=$FONT_NAME splash -x $var_position -y $var_density -dev 005_001_density.pdf -d $splash_defaults -l $splash_limits ../output/00000001.csv


# Step 6: initial pressure and sound speed
# ---------

echo '----------- Step 6: initial pressure and sound speed -----------'
echo 'Initial pressure' > splash.titles
GIZA_FONT=$FONT_NAME splash -x $var_position -y $var_pressure -dev 006_001_pressure.pdf -d $splash_defaults -l $splash_limits ../output/00000001.csv

echo 'Initial sound speed' > splash.titles
GIZA_FONT=$FONT_NAME splash -x $var_position -y $var_sound_speed -dev 006_002_c_sound.pdf -d $splash_defaults -l $splash_limits ../output/00000001.csv

echo 'We are done'

