!
! Functions that calculate thermodynamical variables: presssure,
! internal energy etc.
!
module Thermodynamics
use Types, only: dp

implicit none
private
public :: get_internal_energy, get_pressure, get_pressure_isothermal

contains

real(dp) elemental function get_internal_energy(gamma, pressure, density) &
        result(internal_energy)

    real(dp), intent(in) :: gamma
    real(dp), intent(in) :: pressure
    real(dp), intent(in) :: density

    ! From adiabatic equation of state
    !   P = (gamma - 1) * rho * u
    internal_energy = pressure / ((gamma - 1.0_dp) * density)
end function

real(dp) elemental function get_pressure_isothermal(density, sound_speed) &
    result(pressure)

    real(dp), intent(in) :: density
    real(dp), intent(in) :: sound_speed

    pressure = (sound_speed**2.0_dp) * density
end function

real(dp) elemental function get_pressure(gamma, density, internal_energy) &
    result(pressure)

    real(dp), intent(in) :: gamma
    real(dp), intent(in) :: density
    real(dp), intent(in) :: internal_energy

    ! From adiabatic equation of state
    !   P = (gamma - 1) * rho * u
    pressure = (gamma - 1) * density * internal_energy
end function

end module Thermodynamics
