module GhostTest
use Types, only: dp
use AssertsTest, only: assert_true, assert_approx, assert_all_approx, assert_equal
use Settings, only: program_settings, allocate_settings
use Properties, only: program_properties
use Grid, only: set_grid

use Variables, only: vars_count, &
                     var_position, &
                     var_speed, &
                     var_mass, &
                     var_smoothing_length, &
                     var_density, &
                     var_internal_energy, &
                     var_pressure, &
                     var_sound_speed

use Ghost, only: update_ghosts


implicit none
private
public ghost_test_all

contains

subroutine update_ghosts_test(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    type(program_properties) :: props
    real(dp), allocatable :: vars(:, :), a(:)

    ! Set settings
    ! -------

    call allocate_settings(options)
    props%n_real = 5
    options%x_initial_min = 0._dp
    options%x_initial_max = 10._dp
    options%spline_zero_point = 2._dp

    ! Allocate variables
    ! -------

    call set_grid(n_particles=10, vars=vars)
    allocate(a(size(vars, 2)))

    ! Set variables
    ! -------

    vars(var_position, 1:5) = [1._dp, 3._dp, 5._dp, 7._dp, 9._dp]
    vars(var_mass, 1:5) = [3.1_dp, 3.2_dp, 3.3_dp, 3.4_dp, 3.5_dp]
    vars(var_smoothing_length, 1:5) = [2._dp, 1._dp, 2._dp, 2._dp, 2._dp]

    call update_ghosts(options=options, vars=vars, props=props)

    call assert_equal(props%n_ghosts, 3,  __FILE__, __LINE__, failures)

    ! Real particles
    ! ---------

    ! Positions
    a = vars(var_position, :)

    call assert_approx(a(1), 1._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(2), 3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(3), 5._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(4), 7._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(5), 9._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! ghosts

    call assert_approx(a(6), 11._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(7), -3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(8), -1._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)


    ! masses
    a = vars(var_mass, :)

    call assert_approx(a(1), 3.1_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(2), 3.2_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(3), 3.3_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(4), 3.4_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(5), 3.5_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! ghosts
    call assert_approx(a(6), 3.1_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(7), 3.4_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(8), 3.5_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! Reserved particles are unchanged
    ! --------

    call assert_all_approx(vars(var_position, 9:), 42.01_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_mass, 9:), 42.01_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)
end


subroutine update_ghosts_test__no_ghosts(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    type(program_properties) :: props
    real(dp), allocatable :: vars(:, :), a(:)

    ! Set settings
    ! -------

    call allocate_settings(options)
    props%n_real = 5
    options%x_initial_min = 0.0_dp
    options%x_initial_max = 10.0_dp
    options%spline_zero_point = 2._dp

    ! Allocate variables
    ! -------

    call set_grid(n_particles=10, vars=vars)
    allocate(a(size(vars, 2)))

    ! Set variables
    ! -------

    vars(var_position, 1:5) = [1._dp, 3._dp, 5._dp, 7._dp, 9._dp]
    vars(var_mass, 1:5) = [3.1_dp, 3.2_dp, 3.3_dp, 3.4_dp, 3.5_dp]
    vars(var_smoothing_length, 1:5) = 0.01_dp

    call update_ghosts(options=options, vars=vars, props=props)

    call assert_equal(props%n_ghosts, 0,  __FILE__, __LINE__, failures)

    ! Real particles
    ! ---------

    ! Positions
    a = vars(var_position, :)

    call assert_approx(a(1), 1._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(2), 3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(3), 5._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(4), 7._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(5), 9._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! masses
    a = vars(var_mass, :)

    call assert_approx(a(1), 3.1_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(2), 3.2_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(3), 3.3_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(4), 3.4_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(5), 3.5_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! Reserved particles are unchanged
    ! --------

    call assert_all_approx(vars(var_position, 6:), 42.01_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_mass, 6:), 42.01_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)
end


subroutine update_ghosts_test__no_ghosts_flag(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    type(program_properties) :: props
    real(dp), allocatable :: vars(:, :), a(:)

    ! Set settings
    ! -------

    call allocate_settings(options)
    options%no_ghosts = .true.
    props%n_ghosts = 123

    ! Allocate variables
    ! -------

    call set_grid(n_particles=10, vars=vars)
    allocate(a(size(vars, 2)))

    call update_ghosts(options=options, vars=vars, props=props)

    call assert_equal(props%n_ghosts, 0,  __FILE__, __LINE__, failures)

    ! Reserved particles are unchanged
    ! --------

    call assert_all_approx(vars(var_position, 6:), 42.01_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_mass, 6:), 42.01_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)
end

subroutine ghost_test_all(failures)
    integer, intent(inout) :: failures

    call update_ghosts_test(failures)
    call update_ghosts_test__no_ghosts(failures)
    call update_ghosts_test__no_ghosts_flag(failures)
end

end module GhostTest
