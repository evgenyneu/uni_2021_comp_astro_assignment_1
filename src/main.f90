! The entry point to the program
program Main
use Simulation, only: read_settings_and_run
implicit none

call read_settings_and_run(silent=.false.)

end program Main
