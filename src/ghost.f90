!
! Adds ghost particles
!
module Ghost
use Types, only: dp
use Settings, only: program_settings
use Properties, only: program_properties
use String, only: str
use Grid, only: UNASSIGNED_VAR

use Variables, only: vars_count, &
                     var_position, &
                     var_speed, &
                     var_mass, &
                     var_smoothing_length, &
                     var_density, &
                     var_internal_energy, &
                     var_pressure, &
                     var_sound_speed

implicit none
private
public :: update_ghosts

contains

!
! Clones i-th particle and adds it as a ghost.
!
! Inputs:
! -------
!
! n : number of real particles.
!
! dx : width of the domain (x_max - x_min).
!
! i : index of the particle in `vars` array. This is the particle
!     we make a ghost clone of and teleport to the other side of the box.
!
! Outputs:
! -------
!
! vars : A 2D array containing the values of variables for all particles:
!           * Index 1: type of the variable, described in variables.f90.
!           * Index 2: particle number.
!
! n_ghosts : number of ghost particles.
!
subroutine add_ghost(n, dx, i, vars, n_ghosts)
    integer, intent(in) :: n, i
    real(dp), intent(in) :: dx
    real(dp), allocatable, intent(inout) :: vars(:, :)
    integer, intent(inout) :: n_ghosts
    n_ghosts = n_ghosts + 1

    if (n + n_ghosts > size(vars, 2)) then
        write (0, *) "Array is too small to fit "//trim(str(n_ghosts)) &
//" ghosts. &
&Increase n_particles_max setting."
        call exit(423)
    end if

    vars(:, n + n_ghosts) = vars(:, i) ! copy all variables to the ghost

    ! Teleport the ghost to other side of the box
    vars(var_position, n + n_ghosts) = vars(var_position, i) - dx
end subroutine

!
! Adds ghost particles to each side of the box which are copies
! of corresponding real particles from the other side.
!
! Inputs:
! -------
!
! options : program options
!
! Outputs:
! -------
! vars : A 2D array containing the values of variables for all particles:
!           * Index 1: type of the variable, described in variables.f90.
!           * Index 2: particle number.
!
! props : parameters that change during the simulation.
!
subroutine update_ghosts(options, vars, props)
    type(program_settings), intent(in) :: options
    type(program_properties), intent(inout) :: props
    real(dp), allocatable, intent(inout) :: vars(:, :)
    integer :: i

    ! Declare shortcut variables
    ! --------------

    integer :: n ! number of real particles
    real(dp) :: x ! position
    real(dp) :: h ! smoothing length
    real(dp) :: x_min, x_max ! maximum and minimum values of x
    real(dp) :: dx ! width of the domain

    ! Assign and allocate shortcut variables
    ! --------------

    n = props%n_real
    x_min = options%x_initial_min
    x_max = options%x_initial_max
    dx = x_max - x_min
    props%n_ghosts = 0

    if (options%no_ghosts) return

    ! Set special value for unasigned particles
    ! COuld be useful later to track bugs if we use values of unasigned particles
    vars(:, n + 1:) = UNASSIGNED_VAR

    do i = 1, n
        x = vars(var_position, i)
        h = vars(var_smoothing_length, i)

        if (x + options%spline_zero_point * h > x_max) then
            call add_ghost(n=n, dx=dx, i=i, vars=vars, n_ghosts=props%n_ghosts)
        else if (x - options%spline_zero_point * h < x_min) then
            call add_ghost(n=n, dx=-dx, i=i, vars=vars, n_ghosts=props%n_ghosts)
        end if
    end do
end subroutine


end module Ghost
