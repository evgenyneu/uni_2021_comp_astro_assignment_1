!
! Calculates the spline function.
!
module Spline
use Types, only: dp

implicit none
private
public :: get_spline


contains


!
! Calculate cubic spline at specific point.
!
! Inputs:
! -------
!
! x : argument of the pline function, can be a number or an array.
!
! keep_elements : the number of elements to keep in resized array
!
!
! Outputs:
! -------
!
! Returns : value of the spline fuction, or -1 in case of error. If `x`
!           is an array the return value is also an array.
!
real(dp) elemental function get_spline(x) result(y)
    real(dp), intent(in) :: x

    if (x >= 0 .and. x < 1) then
        y = 0.25_dp * (2 - x)**3 - (1 - x)**3
    else if (x >= 1 .and. x < 2) then
        y = 0.25_dp * (2 - x)**3
    else if (x >= 2) then
        y = 0
    else
        y = -1 ! Error: can't be negative
    end if

    y = (2.0_dp / 3.0_dp) * y
end function

end module Spline
