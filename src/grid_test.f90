module GridTest
use Grid, only: set_grid
use Types, only: dp
use Settings, only: program_settings, allocate_settings
use AssertsTest, only: assert_true, assert_approx, assert_equal
implicit none
private
public grid_test_all

contains


subroutine set_grid_test(failures)
    integer, intent(inout) :: failures
    real(dp), allocatable :: vars(:, :)

    call set_grid(n_particles=101, vars=vars)

    call assert_equal(size(vars, 1), 8, __FILE__, __LINE__, failures)
    call assert_equal(size(vars, 2), 101, __FILE__, __LINE__, failures)

    ! Check default values are set
    call assert_true(all(abs(vars - 42.01_dp) < 1.e-10_dp), __FILE__, &
        __LINE__, failures)
end


subroutine grid_test_all(failures)
    integer, intent(inout) :: failures

    call set_grid_test(failures)
end

end module GridTest
