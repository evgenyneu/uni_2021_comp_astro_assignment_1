!
! Functions that help to work with strings
!
module String
use Types, only: dp
use FloatUtils, only: is_zero
implicit none
private

public :: string_starts_with, string_contains, &
          string_is_empty, string_to_number, str, split_string, index_from, &
          join_strings


interface string_to_number
    module procedure string_to_real_dp, string_to_integer
end interface

!
! Convert number to string.
!
! Inputs:
! --------
!
! number : a number.
!
! round : number of decimal points to show (for real numbers only)
!
! scientific : if .true. show number in scientific format, i.e. 1.23E-3
!
! Outputs:
! -------
!
! Returns: string representation of the number.
!
interface str
    module procedure str_integer, str_real_dp
end interface

contains


!
! Returns the index of the first occurence of substring within the string,
! only searching from given index
!
! Inputs:
! --------
!
! str : a string.
!
! substring : a substring to search for
!
! start : an integer index, occurence of substring is searched only starting
!         from this index in the string.
!
! Outputs:
! -------
!
! Returns: index of first occurence of the substring. Zero if not found.
!
function index_from(str, substr, start) result(result)
    character(len=*), intent(in) :: str
    character(len=*), intent(in) :: substr
    integer, intent(in) :: start
    integer :: result
    result = index(str(start:), substr)
    if (result /= 0) result = result + start - 1
end function


!
! Splits a string into multiple strings given a separator substring.
!
! Example
! --------
! Split "one,two,three" into ["one", "two", "three"] for "," separator.
!
!   character(len=100), allocatable :: splitted(:)
!   split_string("one,two,three", ",", splitted)
!
! Inputs:
! --------
!
! str : a string to to be splitted
! separator : a separator.
!
! Outputs:
! -------
!
! splitted : array of splitted substrings.
!
subroutine split_string(str, separator, splitted)
    character(len=*), intent(in) :: str, separator
    character(len=*), allocatable, intent(out) :: splitted(:)
    character(len=len(splitted(:))), allocatable :: splitted_tmp(:)
    integer :: i ! Current index for separator
    integer :: i_prev ! Previous index for separator
    integer :: n_strings ! Current number of splitted strings
    i = 1
    i_prev = 1
    n_strings = 1

    allocate(splitted_tmp(max(len(str), 1) + 1))

    do while (i /= 0)
        i = index_from(str, separator, i_prev)

        if (i == 0) then
            splitted_tmp(n_strings) = str(i_prev:)
        else
            splitted_tmp(n_strings) = str(i_prev: i - 1)
            i_prev = i + len(separator)
            n_strings = n_strings + 1
        end if
    end do

    splitted = splitted_tmp(1:n_strings)
end subroutine


!
! Joins all strings from the supplied array into one string using the separator.
!
! Inputs:
! --------
!
! strings : array of strings
!
! separator : a separator that will be inserted between the strings.
!
! Outputs:
! -------
!
! joined: string cotaining the joined strings from the array.
!
subroutine join_strings(strings, separator, joined)
    character(len=*), intent(in) :: strings(:)
    character(len=*), intent(in) :: separator
    character(len=*), intent(out) :: joined
    integer :: i
    joined = ""

    do i = 1, size(strings)
        if (i == 1) then
            joined = strings(i)
        else
            joined = trim(joined)//separator//strings(i)
        end if
    end do
end subroutine

!
! Check if a string starts with a given substring
!
! Inputs:
! --------
!
! str : a string.
!
! substring : a substring.
!
! Outputs:
! -------
!
! Returns: .true. if `str` starts with `substring`.
!
function string_starts_with(str, substring) result(result)
    character(len=*), intent(in) :: str, substring
    logical :: result

    result = index(str, substring) == 1
end function


!
! Check if a string contains another string
!
! Inputs:
! --------
!
! str : a string.
!
! substring : a substring.
!
! Outputs:
! -------
!
! Returns: .true. if `str` contains `substring`.
!
function string_contains(str, substring) result(result)
    character(len=*), intent(in) :: str, substring
    logical :: result

    result = index(str, substring) /= 0
end function


!
! Check if a string is empty (zero length or contains spaces)
!
! Inputs:
! --------
!
! str : a string.
!
! Outputs:
! -------
!
! Returns: .true. if `str` is empty
!
function string_is_empty(str) result(result)
    character(len=*), intent(in) :: str
    logical :: result

    result = len(trim(str)) == 0
end function


!
! Convert a string to a real double precision number
!
! Inputs:
! --------
!
! str : a string
!
! Outputs:
! -------
!
! success : .true. if conversion was successful.
!
! number: the number converted from `str`
!
subroutine string_to_real_dp(str, number, success)
    character(len=*), intent(in) :: str
    real(dp), intent(out) :: number
    logical, intent(out) :: success
    integer :: iostat

    read(str, *, iostat = iostat) number
    success = iostat == 0
end subroutine

!
! Convert a string to an integer number
!
! Inputs:
! --------
!
! str : a string
!
! Outputs:
! -------
!
! success : .true. if conversion was successful.
!
! number: the number converted from `str`
!
subroutine string_to_integer(str, number, success)
    character(len=*), intent(in) :: str
    integer, intent(out) :: number
    logical, intent(out) :: success
    integer :: iostat

    read(str, *, iostat = iostat) number
    success = iostat == 0
end subroutine

function str_integer(number) result(result)
    integer, intent(in) :: number
    character(len=20) :: result
    write(result, "(I20)") number
    result = adjustl(trim(result))
end function

function str_real_dp(number, round, scientific) result(result)
    real(dp), intent(in) :: number
    integer, intent(in), optional :: round
    logical, intent(in), optional :: scientific
    integer :: round_v
    logical :: scientific_v
    integer :: i

    character(len=100) :: result
    character(len=4) :: str_format
    character(len=4) :: str_format_exp

    str_format = "F"
    str_format_exp = ""

    if(present(round)) then
        round_v = round
    else
        round_v = 2

        if (is_zero(number - nint(number)) .and. number < 100000) then
            result = str_integer(nint(number))
            return
        end if

        ! Determine rounding automatically
        do i = 1, 3
            if (is_zero(number * (10**i) - nint(number * (10**i)))) then
                round_v = i
                exit
            end if
        end do
    end if

    if(present(scientific)) then
        scientific_v = scientific
    else
        scientific_v = number < .1
    end if

    if (scientific_v) then
        str_format = "ES"
        str_format_exp = "E2"
    end if

    write(result, "("//str_format//"10."//str_integer(round_v)//str_format_exp//")") number
    result = adjustl(trim(result))
end function

end module String
