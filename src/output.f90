!
! Prints out values of variables for all particles into a text file
!
module Output

use Types, only: dp
use String, only: str, join_strings
use Settings, only: program_settings
use Properties, only: program_properties
use FileUtils, only: create_dir
use Variables, only: vars_count, var_names

implicit none
private

public :: write_output, make_file_name


contains


!
! Prints out values of variables for all particles into a text file.
!
! Inputs:
! --------
!
! options : program options
!
! props : parameters that change during the simulation.
!
! file_number : a number used in the file name, padded with zeros on the left.
!
! time : the value of time variable.
!
! vars : A 2D array containing the values of variables for all particles:
!           * Index 1: type of the variable, described in variables.f90.
!           * Index 2: particle number.
!
!
subroutine write_output(options, props, file_number, time, vars)
    type(program_settings), intent(in) :: options
    type(program_properties), intent(in) :: props
    integer, intent(in) :: file_number
    real(dp), intent(in) :: time
    real(dp), allocatable, intent(in) :: vars(:, :)
    integer :: file_unit
    integer :: i
    character(len=1000 * 10) :: file_path
    character(len=1000) :: file_name
    character(len=1000) :: numbers_format
    character(len=1000 * 10) :: var_names_str
    character(len=1000) :: fmt

    call create_dir(options%output_dir)

    call make_file_name(options=options, is_text=.true., &
                        file_number=file_number, name=file_name)

    file_path = trim(options%output_dir) // '/' // trim(file_name)

    open(newunit=file_unit, file=trim(file_path), status="replace")

    ! Print out the names of the variables
    ! ---------

    call join_strings(var_names, ", ", var_names_str)
    write(file_unit, '(a)') trim(var_names_str)

    ! Print out time variable
    write(file_unit, '('//trim(options%real_dp_output_format)//')') time

    fmt = trim(options%real_dp_output_format)

    ! Separate variables with commas (CSV)
    numbers_format = '('//trim(fmt)//', '//str(vars_count-1)&
                     //'(",", '//trim(fmt)//'))'

    ! Loop over real particles
    do i = 1, props%n_real
        ! Print out all variables for the i-th real particle
        write(file_unit, numbers_format) vars(:, i)
    end do

    close(unit=file_unit)
end subroutine


!
! Makes a file name for the output file based on the file_number.
!
! Example:
! --------
!
! Will return
!   name='0000032.csv'
! for
!   options%output_file_basename_length=7
!   is_text=.true.
!   file_number=32
!
! Inputs:
! --------
!
! options : program options
!
! is_text : If .true. then file name will end with '.csv'. If false, then
!           it will end with '.bin'.
!
! file_number : a number used in the file name, padded with zeros on the left.
!
! Output:
! --------
!
! name : file name.
!
subroutine make_file_name(options, is_text, file_number, name)
    type(program_settings), intent(in) :: options
    logical, intent(in) :: is_text
    integer, intent(in) :: file_number
    character(len=*), intent(out) :: name
    character(len=3) :: extension
    integer :: len

    if (is_text) then
        extension = 'csv'
    else
        extension = 'bin'
    end if

    len = options%output_file_basename_length
    write(name, "(i0."//str(len)//",'.',a)") file_number, trim(extension)
end subroutine

end module Output
