!
! Helper functions to deal with file system
!
module FileUtils

#ifdef __INTEL_COMPILER
! ifport is needed to use system() function with ifort compiler,
! system() is already included with gfortran
use ifport
#endif

implicit none
private
public :: file_exists, delete_file, delete_dir, create_dir

contains

!
! Returns .true. if file or directory at a given path exists
!
! Inputs:
! -------
!
! filename : Path to a file.
!
!
! Outputs:
! -------
!
! Returns : .true. if file exists.
!
function file_exists(filename) result(res)
  character(len=*),intent(in) :: filename
  logical :: res

  inquire(file=trim(filename), exist=res)
end function


!
! Delete a file.
!
! Inputs:
! -------
!
! filename : Path to a file.
!
subroutine delete_file(filename)
    character(len=*), intent(in) :: filename
    integer :: stat

    open(unit=1234, iostat=stat, file=trim(filename), status='old')
    if (stat == 0) close(1234, status='delete')
end subroutine


!
! Deletes a directory.
!
! Inputs:
! -------
!
! filename : Path to a directory to be deleted
!
subroutine delete_dir(path)
    character(len=*), intent(in) :: path
    integer :: stat

    if (len(trim(path)) < 5) then
        write (0, *) "Can not delete directory that is shorter than 5 &
&for safety reasons: " // trim(path)
        call exit(432)
    end if

    stat = system('rm -rf ' // "'" // trim(path) // "'")

    if (stat /= 0) then
        write (0, *) "Failed to delete directory '" // trim(path) // "'"
        call exit(143)
    endif
end subroutine


!
! Create a directory.
!
! Inputs:
! -------
!
! path : Path to a directory. Can contains sub directories, example:
!        call create_dir("mydir/sub1/sub2")
!
subroutine create_dir(path)
    character(len=*), intent(in) :: path
    integer :: stat
#ifdef _WIN32
    character(len=*),parameter :: MKDIR = 'mkdir '
#else
    character(len=*),parameter :: MKDIR = 'mkdir -p '
#endif

    stat = system(MKDIR // "'" // trim(path) // "'")

    if (stat /= 0) then
        write (0, *) "Failed to create directory '" // trim(path) // "'"
        call exit(183)
    endif

end subroutine


end module FileUtils
