module SettingsTest
use Types, only: dp

use AssertsTest, only: assert_equal, assert_true, assert_approx, &
                       assert_string_starts_with, assert_string_contains

use CommandLineArgs, only: parsed_args, &
                           parse_command_line_arguments_from_str

use Settings, only: read_from_command_line, read_from_parsed_command_line, &
                    program_settings

use CommandLineAddArg, only: add_flag, add_positional_arg, add_arg, &
                             program_args

use String, only: string_starts_with, string_is_empty
implicit none
private

public settings_test_all

contains

! read_from_parsed_command_line
! ----------------

subroutine read_from_parsed_command_line_test__no_args(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_settings) :: settings
    character(len=1024) :: error_message
    logical :: success

    call parse_command_line_arguments_from_str("", parsed)

    call read_from_parsed_command_line(parsed=parsed, &
                                       settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_true(.not. settings%help, __FILE__, __LINE__, failures)

    call assert_equal(settings%output_dir, 'output', &
                      __FILE__, __LINE__, failures)

    call assert_equal(settings%n_particles_max, 1000, __FILE__, &
                      __LINE__, failures)

    call assert_equal(settings%n_particles_initial, 100, __FILE__, &
                      __LINE__, failures)

    call assert_approx(settings%x_initial_min, 0.0_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%x_initial_max, 1.0_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%density_initial, 1.0_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%sound_speed_initial, 1.0_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%speed_amplitude_factor_initial, &
                       0.0001_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%smoothing_length_factor_initial, &
                       1.2_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_equal(settings%initial_conditions, 'sine', &
                      __FILE__, __LINE__, failures)

    call assert_equal(settings%output_file_basename_length, 8, &
                      __FILE__, __LINE__, failures)

    call assert_equal(settings%real_dp_output_format, 'es23.16', &
                      __FILE__, __LINE__, failures)

    call assert_approx(settings%spline_zero_point, &
                       2._dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_true(.not. settings%no_ghosts, __FILE__, __LINE__, failures)
end

subroutine read_from_parsed_command_line_test__named(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_settings) :: settings
    character(len=1024) :: error_message
    logical :: success
    character(len=1000) :: args_str

    args_str = "&
&--output_dir=test_out_dir &
&--n_particles_max=321 &
&--n_particles_initial=71 &
&--x_initial_min=0.2 &
&--x_initial_max=1.2 &
&--density_initial=0.32 &
&--sound_speed_initial=2.61 &
&--speed_amplitude_factor_initial=4.81 &
&--smoothing_length_factor_initial=5.12 &
&--initial_conditions=shock_tube &
&--output_file_basename_length=12 &
&--real_dp_output_format=f3.4 &
&--spline_zero_point=3.31 &
&--no_ghosts &
&"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call read_from_parsed_command_line(parsed=parsed, settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_true(.not. settings%help, __FILE__, __LINE__, failures)

    call assert_equal(settings%output_dir, 'test_out_dir', &
                      __FILE__, __LINE__, failures)

    call assert_equal(settings%n_particles_max, 321, __FILE__, &
                      __LINE__, failures)

    call assert_equal(settings%n_particles_initial, 71, __FILE__, &
                      __LINE__, failures)

    call assert_approx(settings%x_initial_min, 0.2_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%x_initial_max, 1.2_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%density_initial, 0.32_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%sound_speed_initial, 2.61_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%speed_amplitude_factor_initial, &
                       4.81_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(settings%smoothing_length_factor_initial, &
                       5.12_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_equal(settings%initial_conditions, 'shock_tube', &
                      __FILE__, __LINE__, failures)

    call assert_equal(settings%output_file_basename_length, 12, &
                      __FILE__, __LINE__, failures)

    call assert_equal(settings%real_dp_output_format, 'f3.4', &
                      __FILE__, __LINE__, failures)

    call assert_approx(settings%spline_zero_point, 3.31_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_true(settings%no_ghosts, __FILE__, __LINE__, failures)
end


subroutine show_help_test(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_settings) :: settings
    character(len=2024) :: error_message
    logical :: success
    character(len=1000) :: args_str

    args_str = "data.bin --help"
    call parse_command_line_arguments_from_str(args_str, parsed)

    call read_from_parsed_command_line(parsed=parsed, settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    call assert_true(.not. success, __FILE__, __LINE__, failures)

    call assert_true(.not. string_is_empty(error_message), __FILE__, &
                     __LINE__, failures)

    call assert_true(settings%help, __FILE__, __LINE__, failures)

    call assert_string_starts_with(error_message, &
            NEW_LINE('h')//"This program solves 1D fluid equations", &
            __FILE__, __LINE__, failures)
end


! read_from_parsed_command_line
! --------------

subroutine read_from_command_line_test(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: settings
    logical :: success

    call read_from_command_line(silent=.true., settings=settings, success=success)

    call assert_true(success, __FILE__, __LINE__, failures)
end

subroutine read_from_command_line_unrecognized_test(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_settings) :: settings
    character(len=1024) :: error_message
    logical :: success
    character(len=1000) :: args_str

    args_str = "--huh?=123"
    call parse_command_line_arguments_from_str(args_str, parsed)

    call read_from_parsed_command_line(parsed=parsed, settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    call assert_true(.not. success, __FILE__, __LINE__, failures)

    call assert_true(.not. settings%help, __FILE__, __LINE__, failures)

    call assert_string_starts_with(error_message, &
        "ERROR: Unrecognized parameter 'huh?'", __FILE__, __LINE__, failures)

    call assert_true(.true., __FILE__, __LINE__, failures)
end


subroutine settings_test_all(failures)
    integer, intent(inout) :: failures

    call read_from_parsed_command_line_test__no_args(failures)
    call read_from_parsed_command_line_test__named(failures)
    call show_help_test(failures)
    call read_from_command_line_test(failures)
    call read_from_command_line_unrecognized_test(failures)
end

end module SettingsTest
