module SimulationTest
use Types, only: dp
use AssertsTest, only: assert_true

use Simulation, only: read_settings_and_run

implicit none
private
public simulation_test_all

contains


subroutine read_settings_and_run_test(failures)
    integer, intent(inout) :: failures

    call read_settings_and_run(silent=.true.)

    call assert_true(.true., __FILE__, __LINE__, failures)
end


subroutine simulation_test_all(failures)
    integer, intent(inout) :: failures

    call read_settings_and_run_test(failures)
end

end module SimulationTest
