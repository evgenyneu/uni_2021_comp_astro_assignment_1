module InitialConditionsTest
use Types, only: dp
use AssertsTest, only: assert_true, assert_approx, assert_all_approx, &
                       assert_equal

use InitialConditions, only: set_initial, &
                             sine_conditions, &
                             set_shared_initial_conditions, &
                             spread_particles_evenly

use Settings, only: program_settings, allocate_settings
use Properties, only: program_properties

use Variables, only: vars_count, &
                     var_position, &
                     var_speed, &
                     var_mass, &
                     var_smoothing_length, &
                     var_density, &
                     var_internal_energy, &
                     var_pressure, &
                     var_sound_speed

use Grid, only: set_grid

implicit none
private
public init_test_all

contains


subroutine set_shared_initial_conditions_test(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    type(program_properties) :: props
    real(dp), allocatable :: vars(:, :), a(:)

    ! Set settings
    call allocate_settings(options)
    options%n_particles_initial = 4
    options%x_initial_min = 0._dp
    options%x_initial_max = 8._dp
    options%density_initial = 3._dp
    options%sound_speed_initial = 1.2_dp
    options%smoothing_length_factor_initial = 1.0_dp

    ! Allocate variables
    call set_grid(n_particles=10, vars=vars)
    allocate(a(size(vars, 2)))

    call set_shared_initial_conditions(options=options, vars=vars, props=props)

    call assert_equal(props%n_real, 4, __FILE__, __LINE__, failures)
    call assert_equal(props%n_ghosts, 4, __FILE__, __LINE__, failures)

    ! Positions
    ! ----------

    a = vars(var_position, :)

    call assert_approx(a(1), 1._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(2), 3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(3), 5._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(4), 7._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! Ghosts
    call assert_approx(a(5), 9._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(6), 11._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(7), -3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(8), -1._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)


    ! Masses
    ! ----------

    a = vars(var_mass, :)

    call assert_all_approx(a(1:6), 6.0_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)


    ! Densities
    ! ----------

    a = vars(var_density, :)

    call assert_all_approx(a(1:4), 3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! Ghosts
    call assert_all_approx(a(5:8), 3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)


    ! Smoothing length
    ! ----------

    a = vars(var_smoothing_length, :)

    call assert_all_approx(a(1:8), 2._dp, 1e-8_dp, __FILE__, &
        __LINE__, failures)


    ! Sound speed
    ! ----------

    a = vars(var_sound_speed, :)

    call assert_all_approx(a(1:8), 1.2_dp, 1e-8_dp, __FILE__, &
        __LINE__, failures)


    ! Pressure
    ! ----------

    a = vars(var_pressure, :)

    call assert_all_approx(a(1:8), 4.32_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)



    ! Internal energy
    ! ----------

    a = vars(var_internal_energy, :)

    call assert_all_approx(a(1:8), 2.16_dp, 1e-8_dp, __FILE__, &
        __LINE__, failures)


    ! Verify values of unasigned particles are not set
    ! ----------

    call assert_all_approx(vars(var_position, 9:), &
        42.01_dp, 1.e-10_dp, __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_mass, 9:), &
        42.01_dp, 1.e-10_dp, __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_density, 9:), &
        42.01_dp, 1.e-10_dp, __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_sound_speed, 9:), &
        42.01_dp, 1.e-10_dp, __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_smoothing_length, 9:), &
        42.01_dp, 1.e-10_dp, __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_pressure, 9:), &
        42.01_dp, 1.e-10_dp, __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_internal_energy, 9:), &
        42.01_dp, 1.e-10_dp, __FILE__, __LINE__, failures)
end


subroutine sine_conditions_test(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    real(dp), allocatable :: vars(:, :), v(:)

    ! Set settings
    call allocate_settings(options)
    options%n_particles_initial = 4
    options%x_initial_min = 0._dp
    options%x_initial_max = 8._dp
    options%speed_amplitude_factor_initial = 3.0_dp

    ! Allocate variables
    call set_grid(n_particles=10, vars=vars)
    allocate(v(size(vars, 2)))

    ! Set particle values
    vars(var_position, 1) = 1._dp
    vars(var_position, 2) = 3._dp
    vars(var_position, 3) = 5._dp
    vars(var_position, 4) = 7._dp
    vars(var_sound_speed, 1:4) = 2._dp

    call sine_conditions(options=options, vars=vars)

    v = vars(var_speed, :)

    call assert_approx(v(1), 4.242640687119_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(v(2), 4.2426406871_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(v(3), -4.2426406871_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(v(4), -4.2426406871_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! Values for unasigned particles should not be set
    call assert_all_approx(v(5:), 42.01_dp, 1.e-10_dp, &
        __FILE__, __LINE__, failures)
end


subroutine set_initial_test(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    type(program_properties) :: props
    real(dp), allocatable :: vars(:, :), a(:)

    ! Set settings
    call allocate_settings(options)

    options%n_particles_initial = 4
    options%x_initial_min = 0._dp
    options%x_initial_max = 8._dp
    options%density_initial = 3._dp
    options%sound_speed_initial = 1.2_dp
    options%smoothing_length_factor_initial = 1._dp
    options%speed_amplitude_factor_initial = 3._dp
    options%sound_speed_initial = 2._dp

    ! Allocate variables
    call set_grid(n_particles=10, vars=vars)
    allocate(a(size(vars, 2)))

    call set_initial(options=options, vars=vars, props=props)

    call assert_equal(props%n_real, 4, __FILE__, __LINE__, failures)
    call assert_equal(props%n_ghosts, 4, __FILE__, __LINE__, failures)

    ! Positions
    ! ----------

    a = vars(var_position, :)

    call assert_approx(a(1), 1._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(2), 3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(3), 5._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(4), 7._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! Ghosts
    call assert_approx(a(5), 9._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(6), 11._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(7), -3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(8), -1._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)


    ! Densities
    ! ----------

    a = vars(var_density, :)

    call assert_all_approx(a(1:4), 3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! Ghosts
    call assert_all_approx(a(5:8), 3._dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)


    ! Speeds
    ! -----------

    a = vars(var_speed, :)

    call assert_approx(a(1), 4.242640687119_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(a(2), 4.2426406871_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(a(3), -4.2426406871_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(a(4), -4.2426406871_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! ghosts

    call assert_approx(a(5), 4.242640687119_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(6), 4.242640687119_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(7), -4.2426406871_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    call assert_approx(a(8), -4.2426406871_dp, 1e-10_dp, __FILE__, &
        __LINE__, failures)

    ! Verify positions of unasigned particles are not set
    call assert_all_approx(vars(var_position, 9:), 42.01_dp, 1.e-10_dp, &
        __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_density, 9:), 42.01_dp, 1.e-10_dp, &
        __FILE__, __LINE__, failures)

    call assert_all_approx(vars(var_speed, 9:), 42.01_dp, 1.e-10_dp, &
        __FILE__, __LINE__, failures)
end


subroutine spread_particles_evenly_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: x(5)

    call spread_particles_evenly(min=1.0_dp, max=3.0_dp, x=x)
    call assert_approx(x(1), 1.2_dp, 1e-10_dp, __FILE__, __LINE__, failures)
    call assert_approx(x(2), 1.6_dp, 1e-10_dp, __FILE__, __LINE__, failures)
    call assert_approx(x(3), 2.0_dp, 1e-10_dp, __FILE__, __LINE__, failures)
    call assert_approx(x(4), 2.4_dp, 1e-10_dp, __FILE__, __LINE__, failures)
    call assert_approx(x(5), 2.8_dp, 1e-10_dp, __FILE__, __LINE__, failures)

    call spread_particles_evenly(min=0.0_dp, max=1.0_dp, x=x)
    call assert_approx(x(1), 0.1_dp, 1e-10_dp, __FILE__, __LINE__, failures)
    call assert_approx(x(2), 0.3_dp, 1e-10_dp, __FILE__, __LINE__, failures)
    call assert_approx(x(3), 0.5_dp, 1e-10_dp, __FILE__, __LINE__, failures)
    call assert_approx(x(4), 0.7_dp, 1e-10_dp, __FILE__, __LINE__, failures)
    call assert_approx(x(5), 0.9_dp, 1e-10_dp, __FILE__, __LINE__, failures)
end


subroutine init_test_all(failures)
    integer, intent(inout) :: failures

    call set_shared_initial_conditions_test(failures)

    call spread_particles_evenly_test(failures)
    call sine_conditions_test(failures)
    call set_initial_test(failures)
end

end module InitialConditionsTest
