module CommandLineAddArgTest
use Types, only: dp

use AssertsTest, only: assert_equal, assert_true, assert_approx, &
                       assert_string_starts_with, assert_string_contains

use CommandLineAddArg, only: program_args, add_arg

use CommandLineArgs, only: ARGUMENT_MAX_LENGTH


implicit none
private

public command_line_add_arg_test_all

contains

subroutine add_arg_string_test(failures)
    integer, intent(inout) :: failures
    type(program_args), allocatable :: args(:)
    type(program_args) :: arg
    character(len=ARGUMENT_MAX_LENGTH), pointer :: str_value

    call add_arg(args, 'str', 'Descr str', 'hi', str_value, &
                 [character(len=10) :: 'hello', 'b'])

    call assert_equal(size(args), 1, __FILE__, __LINE__, failures)

    arg = args(1)
    call assert_equal(arg%name, "str", __FILE__, __LINE__, failures)
    call assert_equal(arg%description, "Descr str", __FILE__, __LINE__, failures)
    call assert_equal(arg%data_type, "string", __FILE__, __LINE__, failures)
    call assert_equal(arg%type, "named", __FILE__, __LINE__, failures)
    call assert_equal(arg%default_value_string, "hi", __FILE__, __LINE__, failures)

    call assert_equal(size(arg%allowed_values), 2, __FILE__, __LINE__, failures)
    call assert_equal(arg%allowed_values(1), "hello", __FILE__, __LINE__, failures)
    call assert_equal(arg%allowed_values(2), "b", __FILE__, __LINE__, failures)

    arg%parsed_value_string = 'test value 123'
    call assert_equal(str_value, 'test value 123', __FILE__, __LINE__, failures)
end

subroutine add_arg_string_test__no_allowed_values(failures)
    integer, intent(inout) :: failures
    type(program_args), allocatable :: args(:)
    type(program_args) :: arg
    character(len=ARGUMENT_MAX_LENGTH), pointer :: str_value

    call add_arg(args, 'str', 'Descr str', 'hi', str_value)

    call assert_equal(size(args), 1, __FILE__, __LINE__, failures)

    arg = args(1)
    call assert_equal(arg%name, "str", __FILE__, __LINE__, failures)
    call assert_equal(size(arg%allowed_values), 0, __FILE__, __LINE__, failures)
end


subroutine command_line_add_arg_test_all(failures)
    integer, intent(inout) :: failures

    call add_arg_string_test(failures)
    call add_arg_string_test__no_allowed_values(failures)
end

end module CommandLineAddArgTest
