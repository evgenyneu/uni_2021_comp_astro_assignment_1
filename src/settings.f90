!
! Program settings supplied by the user used the command line arguments:
!
!   $ ./build/main --output_dir=output --n_particles_initial=100
!
module Settings
use Types, only: dp
use String, only: string_is_empty

use CommandLineArgs, only: parsed_args, &
                           parse_current_command_line_arguments, &
                           ARGUMENT_MAX_LENGTH, &
                           ERROR_MESSAGE_LENGTH

use CommandLineAddArg, only: add_flag, add_positional_arg, add_arg, &
                             program_args

use CommandLineHelp, only: make_help_message

use CommandLineProcessArgs, only: process_parsed_args

implicit none
private

public :: read_from_parsed_command_line, read_from_command_line, allocate_settings

!
! Stores program settings that are supplied by the user
!
type, public :: program_settings
    ! Is .true. if we want to show help message
    logical, pointer :: help

    ! Path to the output file that will be filled with
    ! solution data
    character(len=ARGUMENT_MAX_LENGTH), pointer :: output_dir

    ! Maximum number of particles
    integer, pointer :: n_particles_max

    ! Initial number of particles
    integer, pointer :: n_particles_initial

    ! Minimum and maximum positions that are used to set positions
    ! of particles at the start of the program
    real(dp), pointer :: x_initial_min
    real(dp), pointer :: x_initial_max

    ! Initial density
    real(dp), pointer :: density_initial

    ! Initial sound speed
    real(dp), pointer :: sound_speed_initial

    ! Amplitude of the initial speeds set as sine wave &
    ! in x direction
    real(dp), pointer :: speed_amplitude_factor_initial

    ! Used for calculating initial smoothing length h for each &
    ! particle, set as this value times particle spacing
    real(dp), pointer :: smoothing_length_factor_initial

    ! Type of initial conditions: 'sine', 'shock_tube'
    character(len=ARGUMENT_MAX_LENGTH), pointer :: initial_conditions

    ! Length of the output file base name
    ! Example: '0032.csv' base name is '0032' and it is four characters long.
    integer, pointer :: output_file_basename_length

    ! The format for double precision floating point numbers used in text
    ! output files. Example: 'es23.16' for output in scientific format
    ! 1.2387000000000001E-07 without loss of precision
    character(len=ARGUMENT_MAX_LENGTH), pointer :: real_dp_output_format

    ! The value of the argument after which the spline returns zero
    real(dp), pointer :: spline_zero_point

    ! Gamma factor in equations of thermodynamics:
    !   gamma = C_P / C_V
    ! where
    !   C_P : specific heat at constant pressure
    !   C_V : specific heat at constant volume
    ! gamma is equal to 5/3 for ideal monoatomic gas.
    real(dp), pointer :: gamma

    ! If .true. then no ghost particles are created
    logical, pointer :: no_ghosts
end type program_settings

! Default values for the settings
! ------

character(len=*), parameter :: DEFAULT_OUTPUT_DIR = "output"
integer, parameter :: DEFAULT_N_PARTICLES_MAX = 1000
integer, parameter :: DEFAULT_N_PARTICLES_INITIAL = 100
real(dp), parameter :: DEFAULT_X_INITIAL_MIN = 0._dp
real(dp), parameter :: DEFAULT_X_INITIAL_MAX = 1._dp
real(dp), parameter :: DEFAULT_DENSITY_INITIAL = 1._dp
real(dp), parameter :: DEFAULT_SOUND_SPEED_INITIAL = 1._dp
real(dp), parameter :: DEFAULT_SPEED_AMPLITUDE_FACTOR_INITIAL = 1.0e-4_dp
real(dp), parameter :: DEFAULT_SMOOTHING_LENGTH_FACTOR_INITIAL = 1.2_dp
real(dp), parameter :: DEFAULT_SPLINE_ZERO_POINT = 2._dp
real(dp), parameter :: DEFAULT_GAMMA = 5._dp / 3._dp
character(len=*), parameter :: DEFAULT_INITIAL_CONDITIONS = "sine"
integer, parameter :: DEFAULT_OUTPUT_FILE_BASENAME_LENGTH = 8
character(len=*), parameter :: DEFAULT_REAL_DP_OUTPUT_FORMAT = "es23.16"

character(len=100), parameter :: ALLOWED_INITIAL_CONDITIONS(2) = &
    [character(len=100) :: 'sine', 'shock_tube']


contains


!
! Allocate program settings and set them to default values.
! This is used for setting up settings in unit test because unit
! tests do not parse command line arguments since they are not run from
! command line.
!
! Inputs:
! -------
!
! settings : program settings
!
subroutine allocate_settings(settings)
    type(program_settings), intent(inout) :: settings

    allocate(settings%help)
    settings%help = .false.

    allocate(settings%output_dir)
    settings%output_dir = "test_dir"

    allocate(settings%n_particles_max)
    settings%n_particles_max = DEFAULT_N_PARTICLES_MAX

    allocate(settings%n_particles_initial)
    settings%n_particles_initial = DEFAULT_N_PARTICLES_INITIAL

    allocate(settings%x_initial_min)
    settings%x_initial_min = DEFAULT_X_INITIAL_MIN

    allocate(settings%x_initial_max)
    settings%x_initial_max = DEFAULT_X_INITIAL_MAX

    allocate(settings%density_initial)
    settings%density_initial = DEFAULT_DENSITY_INITIAL

    allocate(settings%sound_speed_initial)
    settings%sound_speed_initial = DEFAULT_SOUND_SPEED_INITIAL

    allocate(settings%speed_amplitude_factor_initial)
    settings%speed_amplitude_factor_initial = DEFAULT_SPEED_AMPLITUDE_FACTOR_INITIAL

    allocate(settings%smoothing_length_factor_initial)
    settings%smoothing_length_factor_initial = DEFAULT_SMOOTHING_LENGTH_FACTOR_INITIAL

    allocate(settings%initial_conditions)
    settings%initial_conditions = DEFAULT_INITIAL_CONDITIONS

    allocate(settings%output_file_basename_length)
    settings%output_file_basename_length = DEFAULT_OUTPUT_FILE_BASENAME_LENGTH

    allocate(settings%real_dp_output_format)
    settings%real_dp_output_format = DEFAULT_REAL_DP_OUTPUT_FORMAT

    allocate(settings%spline_zero_point)
    settings%spline_zero_point = DEFAULT_SPLINE_ZERO_POINT

    allocate(settings%gamma)
    settings%gamma = DEFAULT_GAMMA

    allocate(settings%no_ghosts)
    settings%no_ghosts = .false.
end subroutine

!
! Reads settings from parsed command line arguments.
! Shows errors to output if command line arguments were incorrect.
!
! Outputs:
! -------
!
! settings : parsed line arguments.
!
! success : .true. if all settings were successfully read and we are ready
!                  to run the program
!
! silent : do not show any output when .true. (used in unit tests)
!
subroutine read_from_command_line(silent, settings, success)
    logical, intent(in) :: silent
    type(program_settings), intent(out) :: settings
    logical, intent(out) :: success
    type(parsed_args) :: parsed
    character(len=ERROR_MESSAGE_LENGTH) :: error_message

    call parse_current_command_line_arguments(parsed)

    call read_from_parsed_command_line(parsed=parsed, settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    if (.not. string_is_empty(error_message)) then
        ! Detected errors in command line arguments
        if (.not. silent) write (0, *) trim(error_message)
        success = .false.
        return
    end if
end subroutine

!
! Reads settings from parsed command line arguments.
!
! Inputs:
! --------
!
! parsed: object containing the parsed command line arguments.
!
!
! Outputs:
! -------
!
! settings : parsed line arguments.
!
! error_message : an error message to be shown to the user. Empty if no error.
!
! success : .true. if there were no errors and we are ready to run the program
!           (i.e. when all required arguments were supplied)
subroutine read_from_parsed_command_line(parsed, settings, error_message, success)
    type(parsed_args), intent(in) :: parsed
    type(program_settings), intent(out) :: settings
    type(program_args), allocatable :: args(:)
    character(len=*), intent(out) :: error_message
    logical, intent(out) :: success
    success = .true.
    error_message = ""

    call add_flag(args, 'help', 'Show this message.', settings%help)

    call add_arg(args, 'output_dir', &
                 'Path to directory where program output will be created.', &
                 DEFAULT_OUTPUT_DIR, settings%output_dir)

    call add_arg(args, 'n_particles_max', &
                 'The maximum number of particles. Used for setting the size &
&of the data array.', &
                 DEFAULT_N_PARTICLES_MAX, settings%n_particles_max)

    call add_arg(args, 'n_particles_initial', &
                 'Initial number of particles.', &
                 DEFAULT_N_PARTICLES_INITIAL, settings%n_particles_initial)

    call add_arg(args, 'x_initial_min', &
                 'The minimum particle position. Used for setting &
&initial positions of the particles.', &
                 DEFAULT_X_INITIAL_MIN, settings%x_initial_min)

    call add_arg(args, 'x_initial_max', &
                 'The maximum particle position. Used for setting &
&initial positions of the particles.', &
                 DEFAULT_X_INITIAL_MAX, settings%x_initial_max)

    call add_arg(args, 'density_initial', &
                 'Initial density.', &
                 DEFAULT_DENSITY_INITIAL, settings%density_initial)

    call add_arg(args, 'sound_speed_initial', &
                 'Initial sound speed.', &
                 DEFAULT_SOUND_SPEED_INITIAL, &
                 settings%sound_speed_initial)

    call add_arg(args, 'speed_amplitude_factor_initial', &
                 'Amplitude of the initial speeds set as sine wave &
&in x direction.', &
                 DEFAULT_SPEED_AMPLITUDE_FACTOR_INITIAL, &
                 settings%speed_amplitude_factor_initial)

    call add_arg(args, 'smoothing_length_factor_initial', &
                 'Used for calculating initial smoothing length h for each &
&particle, set as this value times particle spacing.', &
                 DEFAULT_SMOOTHING_LENGTH_FACTOR_INITIAL, &
                 settings%smoothing_length_factor_initial)

    call add_arg(args, 'initial_conditions', 'Type of initial conditions.', &
                 'sine', &
                 settings%initial_conditions, &
                 ALLOWED_INITIAL_CONDITIONS)

    call add_arg(args, 'output_file_basename_length', &
                 'Length of the output file base name. For example, &
&"0032.csv" file has &
&base name "0032" and it is four characters long.', &
                 DEFAULT_OUTPUT_FILE_BASENAME_LENGTH, &
                 settings%output_file_basename_length)

    call add_arg(args, 'real_dp_output_format', &
                 'The format for double precision floating point numbers used &
&in text output files. For example, use "es23.16" for storing output in &
&scientific format 1.2387000000000001E-07 without loss of precision.', &
                  DEFAULT_REAL_DP_OUTPUT_FORMAT, &
                  settings%real_dp_output_format)

    call add_arg(args, 'spline_zero_point', &
                 'The value of the argument after which the spline is zero.', &
                 DEFAULT_SPLINE_ZERO_POINT, &
                 settings%spline_zero_point)

    call add_arg(args, 'gamma', &
                 'Gamma factor in equations of thermodynamics &
&(gamma = C_P / C_V).', &
                 DEFAULT_GAMMA, &
                 settings%gamma)

    call add_flag(args, 'no_ghosts', 'Do not create ghost particles.', &
                  settings%no_ghosts)

    call process_parsed_args(parsed, args, error_message, success)

    if (settings%help) then
        error_message = make_help_message( &
            'This program solves 1D fluid equations.', &
            './build/main', args, &
            './build/main --output_dir=output --n_particles_initial=100')

        success = .false. ! We show the help message and not running the program
    end if
end subroutine

end module Settings
