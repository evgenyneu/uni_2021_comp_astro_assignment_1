module DensityTest
use Types, only: dp

use AssertsTest, only: assert_true, assert_approx, assert_all_approx, &
                       assert_equal

use Density, only: get_density

use Settings, only: program_settings, allocate_settings

use Grid, only: set_grid

use Variables, only: vars_count, &
                     var_position, &
                     var_speed, &
                     var_mass, &
                     var_smoothing_length, &
                     var_density, &
                     var_internal_energy, &
                     var_pressure, &
                     var_sound_speed

implicit none
private
public density_test_all

contains

subroutine get_density_test(failures)
    integer, intent(inout) :: failures
    real(dp), allocatable :: vars(:, :)
    type(program_settings) :: options
    real(dp) :: result

    ! Set settings
    call allocate_settings(options)
    options%spline_zero_point = 2._dp

    ! Allocate variables
    call set_grid(n_particles=4, vars=vars)

    ! Set variables
    ! -------

    vars(var_position, 1:4) = [1.1_dp, 1.2_dp, 1.3_dp, 2.1_dp]
    vars(var_mass, 1:4) = [3.1_dp, 3.2_dp, 3.3_dp, 3.4_dp]
    vars(var_smoothing_length, 1:4) = [4.1_dp, 4.2_dp, 4.3_dp, 4.4_dp]

    result = get_density(options=options, n_particles=4, vars=vars, a=1)

    call assert_approx(result, &
                       2.068178212288_dp, 1e-10_dp, __FILE__, __LINE__, failures)
end


subroutine density_test_all(failures)
    integer, intent(inout) :: failures

    call get_density_test(failures)
end

end module DensityTest
