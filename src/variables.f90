!
! Names of variables stored for each partiel
!
module Variables
use Types, only: dp
implicit none
private

public  vars_count, &
        var_names, &
        var_position, &
        var_speed, &
        var_mass, &
        var_smoothing_length, &
        var_density, &
        var_internal_energy, &
        var_pressure, &
        var_sound_speed

! Total number of variables stored in the vars array
integer, parameter :: vars_count = 8

! Types of the variables used as the first index of the `vars` array
! (the second index is the particle number)
! ------------

integer, parameter :: var_position = 1
integer, parameter :: var_speed = 2
integer, parameter :: var_mass = 3
integer, parameter :: var_smoothing_length = 4
integer, parameter :: var_density = 5
integer, parameter :: var_internal_energy = 6
integer, parameter :: var_pressure = 7
integer, parameter :: var_sound_speed = 8

character(len=100), parameter :: var_names(vars_count) = &
    [character(len=100) :: &
        'position', &
        'speed', &
        'mass', &
        'smoothing_length', &
        'density', &
        'internal_energy', &
        'pressure', &
        'sound_speed' &
    ]

end module Variables
