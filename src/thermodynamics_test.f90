module ThermodynamicsTest
use Types, only: dp

use AssertsTest, only: assert_true, assert_approx, assert_all_approx, &
                       assert_equal

use Thermodynamics, only: get_internal_energy, get_pressure, &
                          get_pressure_isothermal

implicit none
private
public thermodynamics_test_all

contains

! get_internal_energy
! -----------

subroutine get_internal_energy_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: internal_energy

    internal_energy = get_internal_energy( &
        gamma=5._dp/3._dp, pressure=1.0_dp, density=2.0_dp)

    call assert_approx(internal_energy, &
                       0.75_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)
end

subroutine get_internal_energy_test__with_arrays(failures)
    integer, intent(inout) :: failures
    real(dp) :: internal_energy(2)

    internal_energy = get_internal_energy( &
        gamma = 5._dp / 3._dp, &
        pressure=[1.0_dp, 1.2_dp], &
        density=[2.0_dp, 2.1_dp])

    call assert_approx(internal_energy(1), &
                       0.75_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(internal_energy(2), &
                       0.85714285714285698_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)
end

! get_pressure_isothermal
! -----------

subroutine get_pressure_isothermal_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: pressure

    pressure = get_pressure_isothermal(density=3.0_dp, sound_speed=2.0_dp)

    call assert_approx(pressure, &
                       12.0_dp, 1e-5_dp, __FILE__, __LINE__, failures)
end

subroutine get_pressure_isothermal__with_arrays(failures)
    integer, intent(inout) :: failures
    real(dp) :: pressure(2)

    pressure = get_pressure_isothermal( &
        density=[3.0_dp, 4.0_dp], &
        sound_speed=[2.0_dp, 0.5_dp])

    call assert_approx(pressure(1), &
                       12.0_dp, 1e-5_dp, __FILE__, __LINE__, failures)

    call assert_approx(pressure(2), &
                       1.0_dp, 1e-5_dp, __FILE__, __LINE__, failures)
end

! get_pressure
! -----------

subroutine get_pressure_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: pressure

    pressure = get_pressure( &
        gamma=5._dp / 3._dp, density=3.0_dp, internal_energy=2.0_dp)

    call assert_approx(pressure, &
                       4.0_dp, 1e-5_dp, __FILE__, __LINE__, failures)
end


subroutine thermodynamics_test_all(failures)
    integer, intent(inout) :: failures

    call get_internal_energy_test(failures)
    call get_internal_energy_test__with_arrays(failures)

    call get_pressure_isothermal_test(failures)
    call get_pressure_isothermal__with_arrays(failures)

    call get_pressure_test(failures)
end

end module ThermodynamicsTest
