!
! Register a command line argument
!
module CommandLineAddArg
use Types, only: dp

use CommandLineArgs, only: ARGUMENT_MAX_LENGTH

implicit none
private

public :: add_flag, add_positional_arg, add_arg

! Description of a single command line argiment: its name, description,
! default value etc.
type, public :: program_args
    ! Name of the argument. For example, when using --t_max=2.1
    ! argument, the name is 't_max'
    character(len=:), allocatable :: name

    ! Full description of the argument that will be shown in the program help.
    character(len=:), allocatable :: description

    ! Data type of the argument: logical, string, integer, real_dp
    character(len=:), allocatable :: data_type

    !
    ! The argument type. There are three types:
    !   1. 'named': an argument with a name, for example --t_max=1.2
    !               has a name t_max.
    !
    !   2. 'positional': argument that does not have a name, for example, in
    !                       $ ./program data.bin
    !                    the first argument "data.bin" does not have a name,
    !                    but only a value.
    !
    !   3. 'flag': an argument with a name but without a value,
    !              for example, in
    !                   $ ./program --help
    !               "help" is a flag argument.
    !
    character(len=10) :: type = 'named'

    ! Default values for each of the data types
    ! ----------

    real(dp) :: default_value_real_dp
    integer :: default_value_integer
    character(len=:), allocatable :: default_value_string

    ! Array of strings containing the allowed values for a string argument.
    ! If the array is empty, then all values are allowed.
    character(len=ARGUMENT_MAX_LENGTH), allocatable :: allowed_values(:)

    ! Pointers to variables that will store argument values when
    ! they are parsed by the `process_parsed_args` function.
    real(dp), pointer :: parsed_value_real_dp
    integer, pointer :: parsed_value_integer
    logical, pointer :: parsed_value_logical
    character(len=ARGUMENT_MAX_LENGTH), pointer :: parsed_value_string
end type program_args

!
! Register a single argument.
!
! Inputs:
! --------
!
! name : Name of the argument. For example, when using --t_max=2.1
!        argument, the name is 't_max'.
!
! description : Full description of the argument that will be shown in the program help.
!
! default : default value of the argument that is used when argument is missing.
!
! allowed : Array of strings containing the allowed values for a string argument.
!           If the array is empty, then all values are allowed.
!
! Outputs:
! -------
!
! args : array that stores registered program arguments. This argument will be
!        added to this array.
!
! parsed : pointer to the variable that will store argument value when
!          it's parsed by the `process_parsed_args` function.
!
interface add_arg
    module procedure add_arg_real_dp, &
                     add_arg_integer, &
                     add_arg_string, &
                     add_arg_string__no_allowed
end interface

interface link_args_to_settings
    module procedure link_args_to_settings_real_dp, &
                     link_args_to_settings_integer, &
                     link_args_to_settings_string, &
                     link_args_to_settings_logical
end interface

contains


! link_args_to_settings
! ---------------

subroutine link_args_to_settings_real_dp(arg, settings_value)
    type(program_args), intent(inout) :: arg
    real(dp), pointer, intent(inout) :: settings_value
    allocate(settings_value)
    arg%parsed_value_real_dp => settings_value
end subroutine

subroutine link_args_to_settings_integer(arg, settings_value)
    type(program_args), intent(inout) :: arg
    integer, pointer, intent(inout) :: settings_value
    allocate(settings_value)
    arg%parsed_value_integer => settings_value
end subroutine

subroutine link_args_to_settings_logical(arg, settings_value)
    type(program_args), intent(inout) :: arg
    logical, pointer, intent(inout) :: settings_value
    allocate(settings_value)
    arg%parsed_value_logical => settings_value
end subroutine

subroutine link_args_to_settings_string(arg, settings_value)
    type(program_args), intent(inout) :: arg
    character(len=*), pointer, intent(inout) :: settings_value
    allocate(settings_value)
    arg%parsed_value_string => settings_value
end subroutine

subroutine add_to_args(arg, args)
    type(program_args), intent(in) :: arg
    type(program_args), allocatable, intent(inout) :: args(:)
    type(program_args), allocatable :: args_buffer(:)
    integer :: allocate_result, new_size

    if (.not. allocated(args)) then
        allocate(args(1))
    else
        new_size = size(args) + 1

        ! Add new elemenet to args by reallocating the array
        allocate(args_buffer(new_size), stat=allocate_result)

        if (allocate_result /= 0) then
            write (0, *) "Failed to allocate command line arguments array"
            call exit(32)
        end if

        args_buffer(1:size(args)) = args
        deallocate(args)
        call move_alloc(args_buffer, args)
    end if

    args(size(args)) = arg
end subroutine


!
! Register a single flag argument.
!
! Inputs:
! --------
!
! name : Name of the argument. For example, when using --help
!        argument, the name is 'help'.
!
! description : Full description of the argument that will be shown in the program help.
!
! Outputs:
! -------
!
! args : array that stores registered program arguments. This argument will be
!        added to this array.
!
! parsed : pointer to the variable that will store argument value when
!          it's parsed by the `process_parsed_args` function.
!
subroutine add_flag(args, name, description, parsed)
    type(program_args), allocatable, intent(inout) :: args(:)
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: description
    logical, pointer, intent(inout) :: parsed
    type(program_args) :: arg

    arg%type = 'flag'
    arg%name = name
    arg%description = description
    arg%data_type = 'logical'
    allocate(arg%allowed_values(0))
    call link_args_to_settings(arg, parsed)
    call add_to_args(arg, args)
end subroutine


!
! Register a single positional argument.
!
! Inputs:
! --------
!
! name : Name of the argument. For example, when using --t_max=2.1
!        argument, the name is 't_max'.
!
! description : Full description of the argument that will be shown in the program help.
!
! allowed : Array of strings containing the allowed values for a string argument.
!           If the array is empty, then all values are allowed.
!
! Outputs:
! -------
!
! args : array that stores registered program arguments. This argument will be
!        added to this array.
!
! parsed : pointer to the variable that will store argument value when
!          it's parsed by the `process_parsed_args` function.
!
subroutine add_positional_arg(args, name, description, parsed)
    type(program_args), allocatable, intent(inout) :: args(:)
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: description
    character(len=*), pointer, intent(inout) :: parsed
    type(program_args) :: arg

    arg%type = 'positional'
    arg%name = name
    arg%description = description
    arg%data_type = 'string'
    allocate(arg%allowed_values(0))
    call link_args_to_settings(arg, parsed)
    call add_to_args(arg, args)
end subroutine

! add_arg
! ---------------

subroutine add_arg_real_dp(args, name, description, default, parsed)
    type(program_args), allocatable, intent(inout) :: args(:)
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: description
    real(dp), intent(in) :: default
    real(dp), pointer, intent(inout) :: parsed
    type(program_args) :: arg

    arg%name = name
    arg%description = description
    arg%data_type = 'real_dp'
    arg%default_value_real_dp = default
    allocate(arg%allowed_values(0))
    call link_args_to_settings(arg, parsed)
    call add_to_args(arg, args)
end subroutine

subroutine add_arg_integer(args, name, description, default, parsed)
    type(program_args), allocatable, intent(inout) :: args(:)
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: description
    integer, intent(in) :: default
    integer, pointer, intent(inout) :: parsed
    type(program_args) :: arg

    arg%name = name
    arg%description = description
    arg%data_type = 'integer'
    arg%default_value_integer = default
    allocate(arg%allowed_values(0))
    call link_args_to_settings(arg, parsed)
    call add_to_args(arg, args)
end subroutine

subroutine add_arg_string__no_allowed(args, name, description, default, parsed)
    type(program_args), allocatable, intent(inout) :: args(:)
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: description
    character(len=*), intent(in) :: default
    character(len=*), pointer, intent(inout) :: parsed
    character(len=ARGUMENT_MAX_LENGTH), allocatable :: allowed(:)

    allocate(allowed(0))

    call add_arg_string(args, name, description, default, parsed, allowed)
end subroutine

subroutine add_arg_string(args, name, description, default, parsed, allowed)
    type(program_args), allocatable, intent(inout) :: args(:)
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: description
    character(len=*), intent(in) :: default
    character(len=*), pointer, intent(inout) :: parsed
    character(len=*), intent(in) :: allowed(:)
    type(program_args) :: arg

    arg%name = name
    arg%description = description
    arg%data_type = 'string'
    arg%default_value_string = default
    allocate(arg%allowed_values(size(allowed)))
    arg%allowed_values = allowed

    call link_args_to_settings(arg, parsed)
    call add_to_args(arg, args)
end subroutine


end module CommandLineAddArg
