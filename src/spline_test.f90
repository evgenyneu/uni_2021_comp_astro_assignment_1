module SplineTest
use Types, only: dp

use AssertsTest, only: assert_true, assert_approx, assert_all_approx, &
                       assert_equal

use Spline, only: get_spline

implicit none
private
public spline_test_all

contains

subroutine get_spline_test(failures)
    integer, intent(inout) :: failures

    call assert_approx(get_spline(0.0_dp), &
                       0.667_dp, 1e-2_dp, __FILE__, __LINE__, failures)

    call assert_approx(get_spline(0.5_dp), &
                       0.479_dp, 1e-2_dp, __FILE__, __LINE__, failures)

    call assert_approx(get_spline(1.0_dp), &
                       0.167_dp, 1e-2_dp, __FILE__, __LINE__, failures)

    call assert_approx(get_spline(1.5_dp), &
                       0.021_dp, 1e-2_dp, __FILE__, __LINE__, failures)

    call assert_approx(get_spline(2.0_dp), &
                       0.0_dp, 1e-2_dp, __FILE__, __LINE__, failures)

    call assert_approx(get_spline(2.5_dp), &
                       0.0_dp, 1e-2_dp, __FILE__, __LINE__, failures)
end


subroutine get_spline_test__array(failures)
    integer, intent(inout) :: failures
    real(dp) :: result(2)

    result = get_spline([0.0_dp, 0.5_dp])

    call assert_approx(result(1), &
                       0.667_dp, 1e-2_dp, __FILE__, __LINE__, failures)

    call assert_approx(result(2), &
                       0.479_dp, 1e-2_dp, __FILE__, __LINE__, failures)
end


subroutine spline_test_all(failures)
    integer, intent(inout) :: failures

    call get_spline_test(failures)
    call get_spline_test__array(failures)
end

end module SplineTest
