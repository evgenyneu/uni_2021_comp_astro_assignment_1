!
! Calculates density of a particle
!
module Density
use Types, only: dp
use Spline, only: get_spline
use Settings, only: program_settings

use Variables, only: vars_count, &
                     var_position, &
                     var_speed, &
                     var_mass, &
                     var_smoothing_length, &
                     var_density, &
                     var_internal_energy, &
                     var_pressure, &
                     var_sound_speed

implicit none
private
public :: get_density

contains

!
! Calculate smoothing kernel W.
!
! Inputs:
! -------
!
! q : argument of spline function: abs(r_a - r_b) / h
!
! one_over_ha : quantity 1 / h_a, where h_a is smoothing length for particle
!               for which we calculate the smoothing kernel.
!
! Outputs:
! -------
!
! Returns : value of smoothing kernel.
!
real(dp) elemental function smoothing_kernel(q, one_over_ha) result(W)
    real(dp), intent(in) :: q, one_over_ha

    W = one_over_ha * get_spline(q)
end function


!
! Calculate density of paticle [a] taking in account its neighbours
!
! Inputs:
! -------
!
! options : program options.
!
! n_particles : number of particles including ghosts
!
! vars : A 2D array containing the values of variables for all particles:
!           * Index 1: type of the variable, described in variables.f90.
!           * Index 2: particle number.
!
! a : the index of particle [a] in `vars` array.
!
! Outputs:
! -------
!
! Returns : density of particle [a]
!
real(dp) function get_density(options, n_particles, vars, a) result(rho_a)
    type(program_settings), intent(in) :: options
    integer, intent(in) :: n_particles, a
    real(dp), allocatable, intent(in) :: vars(:, :)
    integer :: b ! Neighbour particle index
    real(dp) :: one_over_ha

    ! Declare shortcut variables
    ! --------------

    real(dp) :: r_a, r_b ! Positions of two particles
    real(dp) :: q ! argument of spline function: abs(r_a - r_b) / h
    real(dp) :: m_b ! Mass of neighbour particle
    real(dp) :: h_a ! Smothing length of our particle

    ! Set values of shortcuts
    ! --------------

    h_a = vars(var_smoothing_length, a)
    one_over_ha = 1.0_dp / h_a
    rho_a = 0

    neighbours: do b = 1, n_particles
        r_a = vars(var_position, a)
        r_b = vars(var_position, b)
        q = abs(r_a - r_b) / h_a

        if (q >= options%spline_zero_point) then
            ! Particle b is too far away - ignore it since
            ! smoothing kernel is zero
            cycle neighbours
        end if

        m_b = vars(var_mass, b)
        rho_a = rho_a + m_b * smoothing_kernel(q=q, one_over_ha=one_over_ha)
    end do neighbours
end function

end module Density
