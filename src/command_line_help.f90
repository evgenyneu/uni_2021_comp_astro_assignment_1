!
! Show help message with all command line options
!
module CommandLineHelp
use Types, only: dp

use CommandLineAddArg, only: program_args
use CommandLineArgs, only: ERROR_MESSAGE_LENGTH
use String, only: str, string_is_empty

implicit none
private

public :: make_help_message

contains

function get_default_value(arg) result(result)
    type(program_args), intent(in) :: arg
    character(len=:), allocatable :: result

    if (arg%type == 'positional' .or. arg%type == 'flag') result = ''

    select case (arg%data_type)
    case ('real_dp')
        result = str(arg%default_value_real_dp)

    case ('integer')
        result = str(arg%default_value_integer)

    case ('string')
        result = arg%default_value_string

    case default
        result = ''
    end select

end function


!
! Make the help message that is shown to the user containing:
!
! 1. Program description.
! 2. Name of executable.
! 3. Description of all arguments and their default values.
! 4. Example of program usage.
!
! Example
! --------
!
! This program solves 1D fluid equations.
!
!     $ ./build/main OUTPUT [OPTIONS]
!
! ARGUMENTS
!
!     OUTPUT: Path to the output data file.
!
!
! OPTIONS
!
!     --help: Show this message.
!
!     --nx: Number of x points in the grid.
!          Default: 100.
!
!     --t_start: The smallest time value.
!          Default: 0.
!
!     --courant_factor: Parameter equal to v*dt/dx.
!          Default: 0.5.
!
!     --initial_amplitude: Parameter A in sine initial conditions.
!          Default: 1.00E-04.
!
!     --initial_conditions: Type of initial conditions.
!          Values: square, sine.
!          Default: square.
!
!
! EXAMPLE

!     $ ./build/main output.bin --nx=100 --initial_conditions=square
!
! Inputs:
! --------
!
! description : Description of the program that will be show at the top.
!
! executable : Name of the executable, that will be shown
!              below the description for example './build/main'
!
! args : array of all arguments used in this program
!
! example : program usage example that will be shown at the bottom, for example
!           './build/main output.bin --nx=100 --initial_conditions=square'
!
!
! Outputs:
! -------
!
! Return : help message that is shown to the user.
!
function make_help_message(description, executable, args, example) result(result)
    character(len=*) :: description, executable, example
    type(program_args), intent(in) :: args(:)
    type(program_args) :: arg
    character(len=ERROR_MESSAGE_LENGTH) :: result
    character(len=:), allocatable :: default
    integer :: i, j, positional_count
    positional_count = 0
    result = NEW_LINE('h') // trim(description)
    result = trim(result) // NEW_LINE('h')
    result = trim(result) // NEW_LINE('h')
    result = trim(result) // "    $ " // executable

    do i = 1, size(args)
        arg = args(i)
        if (arg%type /= 'positional') cycle
        result = trim(result) // ' ' // arg%name
        positional_count = positional_count + 1
    end do

    result = trim(result) // ' [OPTIONS]'

    if (positional_count > 0) then
        result = trim(result) // NEW_LINE('h') // NEW_LINE('h')
        result = trim(result) // 'ARGUMENTS' //  NEW_LINE('h') //  NEW_LINE('h')

        do i = 1, size(args)
            arg = args(i)
            if (arg%type /= 'positional') cycle
            result = trim(result) // "    " // trim(arg%name) // ": "
            result = trim(result) // ' ' // trim(arg%description)
        end do
    end if

    result = trim(result) // NEW_LINE('h') // NEW_LINE('h') // NEW_LINE('h')
    result = trim(result) // 'OPTIONS' //  NEW_LINE('h')


    do i = 1, size(args)
        arg = args(i)
        if (arg%type == 'positional') cycle
        result = trim(result) // NEW_LINE('h')
        result = trim(result) // "    " // "--"
        result = trim(result) // trim(arg%name) // ": "
        result = trim(result) // ' ' // trim(arg%description)

        if (size(arg%allowed_values) > 0) then
            result = trim(result) // NEW_LINE('h')
            result = trim(result) // "         Values: "

            do j = 1, size(arg%allowed_values)
                result = trim(result) // ' ' // trim(arg%allowed_values(j))
                if (j < size(arg%allowed_values)) result = trim(result) // ', '
            end do

             result = trim(result)
        end if

        default = get_default_value(arg)

        if (.not. string_is_empty(default)) then
            result = trim(result) // NEW_LINE('h')
            result = trim(result) // "         Default: "
            result = trim(result) // ' ' // trim(default)
        end if

        result = trim(result) // NEW_LINE('h')
    end do

    result = trim(result) // NEW_LINE('h')
    result = trim(result) // NEW_LINE('h')
    result = trim(result) // 'EXAMPLE' // NEW_LINE('h') // NEW_LINE('h')
    result = trim(result) // "    $ " // example // NEW_LINE('h')
    result = trim(result)
end function

end module CommandLineHelp
