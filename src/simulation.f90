!
! Runs the simulation
!
module Simulation
use Types, only: dp
use Settings, only: program_settings, read_from_command_line
use Properties, only: program_properties
use Output, only: write_output
use Grid, only: set_grid
use InitialConditions, only: set_initial

implicit none
private
public :: run, read_settings_and_run

contains


!
! Runs the simulation
!
!
! Inputs:
! -------
!
! options : program options
!
!
! Outputs:
! -------
!
! props : parameters that change during the simulation
!
subroutine run(options, props)
    type(program_settings), intent(in) :: options
    type(program_properties), intent(inout) :: props
    real(dp), allocatable :: vars(:, :)

    ! Allocate array for storing the variables for all particles
    call set_grid(n_particles=options%n_particles_max, vars=vars)

    ! Set initial conditions
    call set_initial(options=options, vars=vars, props=props)

    ! Print out initial conditions
    call write_output(options=options, props=props, &
                      file_number=1, time=0.0_dp, vars=vars)
end subroutine


!
! Reads program settings from command line arguments
! and runs the simulation
!
! Inputs:
! -------
!
! silent : do not show any output if .true. (used in unit tests)
!
subroutine read_settings_and_run(silent)
    logical, intent(in) :: silent
    type(program_settings) :: settings
    type(program_properties) :: props
    logical :: success

    call read_from_command_line(silent=silent, settings=settings, &
                                success=success)

    if (.not. success) then
        if (.not. silent) call exit(41)
        return
    end if

    call run(options=settings, props=props)

    if (.not. silent) then
        print "(a, a, a)", "Solution saved to '", &
            trim(settings%output_dir), "'"
    end if
end subroutine


end module Simulation
