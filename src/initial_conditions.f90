! Set initial conditions
module InitialConditions
use Types, only: dp
use Constants, only: pi
use Settings, only: program_settings
use Properties, only: program_properties
use Thermodynamics, only: get_internal_energy, get_pressure_isothermal
use Density, only: get_density
use Ghost, only: update_ghosts

use Variables, only: vars_count, &
                     var_position, &
                     var_speed, &
                     var_mass, &
                     var_smoothing_length, &
                     var_density, &
                     var_internal_energy, &
                     var_pressure, &
                     var_sound_speed

use FloatUtils, only: linspace

implicit none
private
public :: set_initial, &
          sine_conditions, &
          set_shared_initial_conditions, &
          spread_particles_evenly


contains


!
! Set positions of particles such that they are spread evenly
! between min and max values when the ends are spliced together.
!
! Example
! -------
!
! For four particles marked as 'x' and min=0 and max=8, we get
! positions at 1, 3, 5 and 7:
!
!          x               x               x               x
!  |-------|-------|-------|-------|-------|-------|-------|-------|
!  0       1       2       3       4       5       6       7       8
!
! The distance between any two neighbours is the same, equal to 2 in this case.
! The key idea is that the distance is also 2 between
! the first and last particles located at 1 and 7 when the ends of the domain
! are connected (which is needed for periodic boundary conditions).
!
! Inputs
! -------
!
! min, max : minumum and maximum values fir the positions.
!
! Outputs
! -------
!
! x : array of particle positions
!
subroutine spread_particles_evenly(x, min, max)
    real(dp) :: min, max
    real(dp), intent(out) :: x(:)
    real(dp) :: dx ! distance between neighbours

    dx = (max - min) / size(x)
    call linspace(min + dx / 2, max - dx / 2, x)
end subroutine


!
! Set initial conditions such as positions for all particles that are used for
! all types of initial conditions (i.e. both for sine and shock_tube)
!
! Inputs:
! -------
!
! options : program settings
!
!
! Outputs:
! -------
!
! vars : A 2D array containing the values of variables for all particles:
!           * Index 1: type of the variable, described in variables.f90.
!           * Index 2: particle number.
!
! props : parameters that change during the simulation.
!
subroutine set_shared_initial_conditions(options, vars, props)
    type(program_settings), intent(in) :: options
    type(program_properties), intent(inout) :: props
    real(dp), allocatable, intent(inout) :: vars(:, :)

    ! Declare shortcut variables
    ! --------------

    integer :: n ! number of real particles
    real(dp), allocatable :: x(:) ! position
    real(dp), allocatable :: m(:) ! mass
    real(dp), allocatable :: h(:) ! smoothing length
    real(dp), allocatable :: rho(:) ! density
    real(dp), allocatable :: c_s(:) ! sound speed
    real(dp), allocatable :: p(:) ! pressure
    real(dp), allocatable :: u(:) ! internal energy
    integer :: i

    ! Assign and allocate shortcut variables
    ! --------------

    n = options%n_particles_initial
    props%n_real = n
    allocate(x(n))
    allocate(m(n))
    allocate(h(n))
    allocate(rho(n))
    allocate(c_s(n))
    allocate(p(n))
    allocate(u(n))

    ! Position
    ! -----------

    call spread_particles_evenly( &
        min=options%x_initial_min, &
        max=options%x_initial_max, &
        x=vars(var_position, 1:n))

    x = vars(var_position, 1:n)

    ! Mass
    ! -----------

    m = (options%x_initial_max - options%x_initial_min) * options%density_initial / n
    vars(var_mass, 1:n) = m

    ! Smoothing length
    ! -----------

    h = options%smoothing_length_factor_initial * (x(2) - x(1))
    vars(var_smoothing_length, 1:n) = h

    ! Density
    ! -----------

    call update_ghosts(options=options, vars=vars, props=props)

    do i = 1, n
        rho(i) = get_density(options=options, &
                             n_particles=n + props%n_ghosts, &
                             vars=vars, a=i)
    end do

    vars(var_density, 1:n) = rho

    ! Sound speed
    ! -----------

    ! Constant sounds speed for isothermal case
    c_s = options%sound_speed_initial
    vars(var_sound_speed, 1:n) = c_s

    ! Pressure
    ! -----------

    p = get_pressure_isothermal(density=rho, sound_speed=c_s)
    vars(var_pressure, 1:n) = p

    ! Internal energy
    ! -----------

    u = get_internal_energy(gamma=options%gamma, pressure=p, density=rho)
    vars(var_internal_energy, 1:n) = u

    call update_ghosts(options=options, vars=vars, props=props)
end subroutine


!
! Set sinusoidal initial condition for particle velocity.
!
! Inputs:
! -------
!
! options : program settings
!
!
! Outputs:
! -------
!
! vars : A 2D array containing the values of variables for all particles:
!           * Index 1: type of the variable, described in variables.f90.
!           * Index 2: particle number.
!
subroutine sine_conditions(options, vars)
    type(program_settings), intent(in) :: options
    real(dp), allocatable, intent(inout) :: vars(:, :)

    ! Declare shortcut variables
    integer :: n ! actual number of particles
    real(dp), allocatable :: c_s(:) ! sound speed
    real(dp), allocatable :: x(:) ! position
    real(dp), allocatable :: x_normalised(:) ! position from 0 to 1
    real(dp), allocatable :: v(:) ! speed
    real(dp), allocatable :: v_a(:) ! speed amplitude

    ! Assign and allocate shortcut variables
    n = options%n_particles_initial
    allocate(c_s(n))
    allocate(x(n))
    allocate(x_normalised(n))
    allocate(v(n))
    allocate(v_a(n))

    ! Assign shortcut variables
    c_s = vars(var_sound_speed, 1:n)
    x = vars(var_position, 1:n)
    v_a = options%speed_amplitude_factor_initial

    ! Set particle speeds using sine wave
    x_normalised = (x - options%x_initial_min) / &
                   (options%x_initial_max - options%x_initial_min)

    v = v_a * c_s * sin(2 * pi * x_normalised)

    ! Assign speed to variables array
    vars(var_speed, 1:n) = v
end subroutine


!
! Calculate initial conditions by setting the values of the variables
! that will be used at the start of the simulation.
!
! Inputs:
! -------
!
! options : program settings
!
!
! Outputs:
! -------
!
! vars : A 2D array containing the values of variables for all particles:
!           * Index 1: type of the variable, described in variables.f90.
!           * Index 2: particle number.
!
! props : parameters that change during the simulation.
!
subroutine set_initial(options, vars, props)
    type(program_settings), intent(in) :: options
    real(dp), allocatable, intent(inout) :: vars(:, :)
    type(program_properties), intent(inout) :: props

    call set_shared_initial_conditions(options=options, vars=vars, props=props)

    select case (options%initial_conditions)
    case ("sine")
        call sine_conditions(options=options, vars=vars)

    case ("shock_tube")
        print "(a)", "&
&ERROR: shock tube initial conditions have not been implemented yet"
       call exit(49)

    case default
       print "(a, a)", "ERROR: unknown initial conditions type ", &
             trim(options%initial_conditions)
       call exit(41)
    end select

    call update_ghosts(options=options, vars=vars, props=props)
end subroutine


end module InitialConditions
