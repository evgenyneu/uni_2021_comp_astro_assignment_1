program MainTest
    use CommandLineArgsTest, only: command_line_args_test_all
    use StringTest, only: string_test_all
    use SettingsTest, only: settings_test_all
    use FloatUtilsTest, only: float_utils_test_all
    use FileUtilsTest, only: file_utils_test_all
    use SimulationTest, only: simulation_test_all
    use OutputTest, only: output_test_all
    use GridTest, only: grid_test_all
    use InitialConditionsTest, only: init_test_all
    use CommandLineAddArgTest, only: command_line_add_arg_test_all
    use CommandLineHelpTest, only: command_line_help_test_all
    use CommandLineProcessArgsTest, only: command_line_process_args_test_all
    use ThermodynamicsTest, only: thermodynamics_test_all
    use SplineTest, only: spline_test_all
    use DensityTest, only: density_test_all
    use GhostTest, only: ghost_test_all
    implicit none

    integer :: failures = 0
    character(len=1024) :: test_word = "TESTS"

    ! Tests for the libraries
    call command_line_args_test_all(failures)
    call command_line_add_arg_test_all(failures)
    call command_line_help_test_all(failures)
    call command_line_process_args_test_all(failures)
    call string_test_all(failures)
    call float_utils_test_all(failures)
    call file_utils_test_all(failures)

    ! Program tests
    call settings_test_all(failures)
    call simulation_test_all(failures)
    call output_test_all(failures)
    call grid_test_all(failures)
    call init_test_all(failures)
    call thermodynamics_test_all(failures)
    call spline_test_all(failures)
    call density_test_all(failures)
    call ghost_test_all(failures)

    if (failures == 0) then
        print *, NEW_LINE('h')//'Tests finished successfully'
    else
        if (failures == 1) then
            test_word = "TEST"
        end if

        print '(a, i4, 1x, a, 1x, a)', NEW_LINE('h'), failures, &
              trim(test_word), 'FAILED'

        call exit(42)
    end if
end program MainTest
