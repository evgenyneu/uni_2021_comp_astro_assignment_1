module FileUtilsTest
use AssertsTest, only: assert_true
use FileUtils, only: file_exists, delete_file, delete_dir, create_dir
implicit none
private
public file_utils_test_all

contains

subroutine file_exists_test(failures)
    integer, intent(inout) :: failures
    logical :: result

    result = file_exists("unknown_file")
    call assert_true(.not. result, __FILE__, __LINE__, failures)

    result = file_exists("src/file_utils.f90")
    call assert_true(result, __FILE__, __LINE__, failures)
end

subroutine delete_file_test(failures)
    integer, intent(inout) :: failures
    integer :: u

    open(newunit=u, file="delete_file_test.txt", status="replace")
    write(u, *) 1, 2
    close(u)

    call assert_true(file_exists("delete_file_test.txt"), &
                     __FILE__, __LINE__, failures)

    call delete_file("delete_file_test.txt")

    call assert_true(.not. file_exists("delete_file_test.txt"), &
                     __FILE__, __LINE__, failures)
end


subroutine create_dir_test(failures)
    integer, intent(inout) :: failures
    integer :: file_unit

    call create_dir("test_dir/sub dir/another one     ")

    open(newunit=file_unit, &
         file="test_dir/sub dir/another one/delete_file_test.txt", &
         status="replace")

    close(file_unit)

    call assert_true(file_exists( &
        "test_dir/sub dir/another one/delete_file_test.txt"), &
        __FILE__, __LINE__, failures)

    call delete_dir("test_dir")

    call assert_true(.not. file_exists( &
        "test_dir/sub dir/another one/delete_file_test.txt"), &
        __FILE__, __LINE__, failures)
end


subroutine file_utils_test_all(failures)
    integer, intent(inout) :: failures

    call file_exists_test(failures)
    call delete_file_test(failures)
    call create_dir_test(failures)
end

end module FileUtilsTest
