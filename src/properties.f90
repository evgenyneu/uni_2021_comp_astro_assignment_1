!
! Stores properties that change during the simulation,
! such as the number of ghost particles.
!
module Properties
use Types, only: dp

implicit none
private

integer, parameter :: UNASSIGNED_PROP_INT = 322412

!
! Properties that change during the simulation.
!
type, public :: program_properties
    integer :: n_real = UNASSIGNED_PROP_INT ! Number of real particles
    integer :: n_ghosts = UNASSIGNED_PROP_INT ! Number of ghost particles
end type

end module Properties
