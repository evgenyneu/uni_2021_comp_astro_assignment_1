!
! Initialize the data array
!
module Grid
use Types, only: dp
use Variables, only: vars_count
implicit none
private

public :: set_grid, UNASSIGNED_VAR

real(dp), parameter :: UNASSIGNED_VAR = 42.01_dp

contains


!
! Initialize the variables array
!
! Inputs:
! -------
!
! n_particles : number of particles we need to store in the variables array.
!               This is equal to maximum number of particles setting
!               n_particles_max supplied by the user.
!
! Outputs:
! -------
! vars : A 2D array containing the values of variables for all particles:
!           * Index 1: type of the variable, described in variables.f90.
!           * Index 2: particle number.
!
subroutine set_grid(n_particles, vars)
    integer, intent(in) :: n_particles
    real(dp), allocatable, intent(out) :: vars(:, :)
    integer :: allocate_result

    ! Allocate array for storing all variables
    allocate(vars(vars_count, n_particles), stat=allocate_result)

    if (allocate_result /= 0) then
        write (0, *) "Failed to allocate variable array"
        call exit(41)
    end if

    ! Unasigned value. Not using zero because I want to spot
    ! problems when unasigned memory is used.
    vars = UNASSIGNED_VAR
end subroutine


end module Grid
