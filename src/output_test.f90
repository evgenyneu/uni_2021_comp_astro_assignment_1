module OutputTest
use Types, only: dp
use Output, only: write_output, make_file_name
use FileUtils, only: file_exists, delete_dir
use AssertsTest, only: assert_true, assert_approx, assert_equal
use Settings, only: program_settings, allocate_settings
use Properties, only: program_properties
use Grid, only: set_grid

use Variables, only: vars_count, &
                     var_position, &
                     var_speed, &
                     var_mass, &
                     var_smoothing_length, &
                     var_density, &
                     var_internal_energy, &
                     var_pressure, &
                     var_sound_speed

implicit none
private
public output_test_all

contains


subroutine write_output_test(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    type(program_properties) :: props
    real(dp), allocatable :: vars(:, :), vars_read(:,:)
    integer :: file_unit
    character(len=1000*10), allocatable :: var_names_str(:)
    real(dp) :: time

    ! Set settings
    call allocate_settings(options)
    props%n_real = 2
    options%output_dir = "test_dir/Output test"
    options%output_file_basename_length = 3

    ! Allocate variables
    call set_grid(n_particles=10, vars=vars)
    call set_grid(n_particles=10, vars=vars_read)
    allocate(var_names_str(vars_count))

    ! Set variables
    ! -------

    vars(var_position, 1:2) = [1.117e-12_dp, 1.217e-12_dp]
    vars(var_speed, 1:2) = [2.1_dp, 2.2_dp]
    vars(var_mass, 1:2) = [3.1_dp, 3.2_dp]
    vars(var_smoothing_length, 1:2) = [4.1_dp, 4.2_dp]
    vars(var_density, 1:2) = [5.1_dp, 5.2_dp]
    vars(var_internal_energy, 1:2) = [6.1_dp, 6.2_dp]
    vars(var_pressure, 1:2) = [7.1_dp, 7.2_dp]
    vars(var_sound_speed, 1:2) = [8.1_dp, 8.2_dp]

    call delete_dir("test_dir")

    call assert_true(.not. file_exists("test_dir/Output test/007.txt"), &
                     __FILE__, __LINE__, failures)

    call write_output(options=options, props=props, file_number=7, &
                      time=0.1_dp, vars=vars)

    call assert_true(file_exists("test_dir/Output test/007.csv"), &
                     __FILE__, __LINE__, failures)

    open(newunit=file_unit, file="test_dir/Output test/007.csv", status="old")

    ! Variable names
    ! --------

    read(file_unit, *) var_names_str

    call assert_equal(var_names_str(1), 'position', &
        __FILE__, __LINE__, failures)

    call assert_equal(var_names_str(2), 'speed', &
        __FILE__, __LINE__, failures)

    call assert_equal(var_names_str(vars_count), 'sound_speed', &
        __FILE__, __LINE__, failures)


    ! Time
    ! --------

    read(file_unit, *) time
    call assert_approx(time, 0.1_dp, 1e-15_dp, __FILE__, __LINE__, failures)

    read(file_unit, *) vars_read(:,1)
    read(file_unit, *) vars_read(:,2)

    ! Particle #1
    ! ----------

    call assert_approx(vars_read(var_position, 1), 1.117e-12_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_speed, 1), 2.1_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_mass, 1), 3.1_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_smoothing_length, 1), 4.1_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_density, 1), 5.1_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_internal_energy, 1), 6.1_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_pressure, 1), 7.1_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_sound_speed, 1), 8.1_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    ! Particle #2
    ! ----------

    call assert_approx(vars_read(var_position, 2), 1.217e-12_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_speed, 2), 2.2_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_mass, 2), 3.2_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_smoothing_length, 2), 4.2_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_density, 2), 5.2_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_internal_energy, 2), 6.2_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_pressure, 2), 7.2_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)

    call assert_approx(vars_read(var_sound_speed, 2), 8.2_dp, &
                       1e-15_dp, __FILE__, __LINE__, failures)


    close(file_unit)
    call delete_dir("test_dir")
end

subroutine make_file_name_test(failures)
    integer, intent(inout) :: failures
    character(len=100) :: result
    type(program_settings) :: options

    call allocate_settings(options)

    ! Text file
    ! -----

    options%output_file_basename_length = 5

    call make_file_name(options=options, is_text=.true., file_number=8, &
                        name=result)

    call assert_equal(result, '00008.csv', __FILE__, __LINE__, failures)

    ! Text file
    ! -----

    options%output_file_basename_length = 4

    call make_file_name(options=options, is_text=.true., file_number=321, &
                        name=result)

    call assert_equal(result, '0321.csv', __FILE__, __LINE__, failures)

    ! Binary file
    ! -----

    options%output_file_basename_length = 4

    call make_file_name(options=options, is_text=.false., file_number=321, &
                        name=result)

    call assert_equal(result, '0321.bin', __FILE__, __LINE__, failures)
end


subroutine output_test_all(failures)
    integer, intent(inout) :: failures

    call write_output_test(failures)
    call make_file_name_test(failures)
end

end module OutputTest
