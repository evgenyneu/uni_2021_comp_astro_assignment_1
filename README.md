# Assignment 1 code

This is code for my assignment 1 for computational astronomy honours unit I did in Monash in semester 1 of 2021.

## Setup

### Download the project

```
git clone https://evgenyneu@bitbucket.org/evgenyneu/uni_2021_comp_astro_assignment_1.git
```

### Change working directory

```
cd uni_2021_comp_astro_assignment_1
```

### Install Fortran

The simulation program is written in Fortran. It can be compiled with either gfortran or ifort. The versions I used were:

* gfortran: gcc version 10.2.0

* ifort: 19.1.1.216


### Install Splash

Install [Splash program](https://splash-viz.readthedocs.io/en/latest/intro.html) that is used to create plots from the output files of the simulation.

#### Mac

```
brew tap danieljprice/all
brew install splash
```

#### Linux

```
sudo apt-get install splash
```

## Compile

```
make
```

This command create the `build/main` program executable. Gfortran compiler is required to build the program. Alternatively, one can uncomment `FC=ifort` and the next `FFLAGS` in the Makefile and compile using ifort.


## Make plots

```
./plot.sh
```

This command

* compiles the Fortran code,

* runs the program which creates output text file in `output` directory, and

* runs Splash program to make the plots in `plots` directory.



## Run unit tests

```
make test
./build/test
```

You will see the following output, if tests are successful:

```
./build/test
 · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · ·
Tests finished successfully
```


## The unlicense

This work is in [public domain](LICENSE).

