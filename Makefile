#
# Makefile for building and testing a fortran program
# ----------------------------------
#
# Build executable file build/main:
#
# 	$ make
#
# Build unit tests executable file build/test:
#
# 	$ make test
#
# Remove build files:
#
# 	$ make clean
#
# This Makefile is based on https://stackoverflow.com/a/30142139/297131
#


# Gfortran compiler
# -------------------

FC=gfortran

# Gfortran compiler flag
# Development: slower execution, faster compilation
# Based on https://staff.washington.edu/rjl/uwamath583s11/sphinx/notes/html/gfortran_flags.html

FFLAGS=-O0 -J$(@D) -Wall -Wextra -g -pedantic -fbacktrace -fbounds-check -ffpe-trap=zero,overflow,underflow

# Gfortran compiler flag
# Production: faster execution, slow compilation

# FFLAGS=-Ofast -J$(@D) -Wall -Wextra -g


# Ifort compiler
# -------------------

# FC=ifort

# Ifort compiler flags  : development (SLOW).
# Development: slower execution, faster compilation.
# Based on https://www.nas.nasa.gov/hecc/support/kb/recommended-intel-compiler-debugging-options_92.html

# FFLAGS=-O0 -module $(@D) -no-wrap-margin -check all -check bounds -check uninit -g -traceback -debug all

# Ifort compiler flags
# Production: faster execution, slow compilation.
#
# For even more optimisation, remove -fp-model precise options
# This may reduce precision and some unit tests may fail.
#
# Based on http://www.bu.edu/tech/support/research/software-and-programming/programming/compilers/intel-compiler-flags/

# FFLAGS=-Ofast -xSSE4.2 -axAVX,CORE-AVX2 -fp-model precise -module $(@D) -no-wrap-margin -g -traceback -debug all


# -------------------


# Libraries to link
# Example: -lm /opt/OpenBLAS/lib/libopenblas.a -lpthread
LINKER_FLAGS =

# Additional directories containing *.h files
# Example: -I/opt/OpenBLAS/include
HEADER_DIRS =

# Binary file name to build
BIN = main

# Put all auto generated stuff to this build dir.
BUILD_DIR = ./build

# List of code files that are general utilities/libraries
# not related to this particular program
CODE_UTILS = types.f90 \
		float_utils.f90 \
		string.f90 \
		asserts_test.f90 \
		float_utils_test.f90 \
		string_test.f90 \
		command_line_args.f90 \
		command_line_args_test.f90 \
		command_line_add_arg.f90 \
		command_line_add_arg_test.f90 \
		command_line_process_args.f90 \
		command_line_process_args_test.f90 \
		command_line_help.f90 \
		command_line_help_test.f90 \
		file_utils.f90 \
		file_utils_test.f90 \
		constants.f90

# List of program Fortran code files
CODE_FILES = \
		variables.f90 \
		settings.f90 \
		properties.f90 \
		settings_test.f90 \
		spline.f90 \
		spline_test.f90 \
		thermodynamics.f90 \
		thermodynamics_test.f90 \
		grid.f90 \
		grid_test.f90 \
		density.f90 \
		density_test.f90 \
		ghost.f90 \
		ghost_test.f90 \
		output.f90 \
		output_test.f90 \
		initial_conditions.f90 \
		initial_conditions_test.f90 \
		simulation.f90 \
		simulation_test.f90 \
		main.f90 \
		main_test.f90

# All code files (utils and program files)
SOURCE_FILES = $(CODE_UTILS) $(CODE_FILES)

# Add the 'src' subdirectory prefix to all file names
FFILES_ALL=$(patsubst %, src/%, $(SOURCE_FILES))

# List all *.f90 files excluding tests
FFILES = $(filter-out %_test.f90, $(FFILES_ALL))

# List all unit test .f90 files
FTESTS = $(filter-out %main.f90, $(FFILES_ALL))

ifeq ($(MAKECMDGOALS), test)
# Build tests
  CFILES_TO_BUILD=$(FTESTS)
  BIN = test
else
# Build the program
  CFILES_TO_BUILD=$(FFILES)
endif

# All .o files go to build dir.
OBJ = $(CFILES_TO_BUILD:%.f90=$(BUILD_DIR)/%.o)

# Default target named after the binary.
$(BIN) : $(BUILD_DIR)/$(BIN)

# Actual target of the binary - depends on all .o files.
$(BUILD_DIR)/$(BIN) : $(OBJ)
# Create build directories - same structure as sources.
	mkdir -p $(@D)
# Link all the object files and make the executable.
	$(FC) $(FFLAGS) $^ -o $@ $(LINKER_FLAGS)


# Build target for every single object file.
$(BUILD_DIR)/%.o : %.f90
	mkdir -p $(@D)
# Complile *.f90 files into the object files.
	$(FC) $(FFLAGS) $(HEADER_DIRS) -cpp -c $< -o $@

.PHONY : clean

clean :
# This should remove all generated files.
	-rm -rf $(BUILD_DIR)
